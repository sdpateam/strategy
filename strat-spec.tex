\section{Strategy}

The strategy component is designed and implemented so that it can control both robots from one computer simultaneously. The component consists of four separate modules: the high-level \textbf{GoalCalculator} which assigns a goal (e.g. ``go get ball") to each robot, the \textbf{planner} responsible for generating a set of actions each robot should perform to achieve its goal and the \textbf{executor} which is then responsible for passing orders through the Communication component. The fourth component, the \textbf{WorldCalculator} performs some post processing of the data passed on by the vision system. All four modules listen to Vision, and their actions are triggered by receiving a new frame from the Vision. Strategy could be potentially extended with a machine learning module which would record the behavior of the enemy teams and provide additional information to the \textbf{GoalCalculator} module. It is also important to mention, that some of the more complex features have been coded, but never actually tested.

\subsection{World Calculator}
Since the Vision module informs its listeners in a fixed order and all of our code is executed sequentially, the World Calculator gets notified with the new frame first. It post-processes the information received from the Vision, namely it: 
\begin{enumerate}
	\item Scales all coordinates of the objects from centimeter granularity to ``tiles" or ``squares" of n*n cm granularity (where n can be dynamically changed). This speeds up our planning algorithms considerably, since there are less possible combinations of actions.
	\item Bundles important constants (e.g. pitch dimensions, enemy defense area coordinates) into the new world object.
	\item Combines information from both Vision and Communication (e.g. the robot acknowledging that it grabbed the ball) to determine which robot actually has the ball.
\end{enumerate}
All of the above information is bundled into a \textit{CalculatedWorld} object which is then used in the subsequent modules.

\subsection{Goal Calculator}
After the data is post-processed we perform a simple analysis of the game situations and adapt to the latest development. The Goal Calculator checks for game situations like turnover or the ball rolling into our defense area and sets goals for both robots accordingly. 
\\For example in a model situation when the ball is stationary in the middle of the pitch, goal for the robot closer to the ball would be to get the ball, while the other one would be ordered to move to a free area of the pitch to receive a pass. After the CalculatedWorld reports we are in possession, robot with the ball would be set to face its teammate, whose goal would be changed to receive a pass.
\\Using an abstract class \textit{Goal} we have implemented this high-level planner so that it is extensible and so that more complex game situations can be modeled easily.

\subsection{Planner}
Every goal can be represented as a sequence of \texttt{Move}, \texttt{Rotate}, \texttt{Synchronise}, \texttt{Grab} and \texttt{Kick} actions. The planner creates a list of objects of class \texttt{Action}, utilizing A* search where necessary. It produces an optimal plan for every frame given the goals for each robot. However it does not always pass the plan further. To prevent frequent changes of plan resulting in a choppy movement of the robots, the planner evaluates whether the old plan, which is currently being executed, is still close to the new optimal one. Only in case the new plan is dramatically different, we push the plan to the next stage of the Strategy component, the \textbf{executor}.

\subsubsection{Obstacle avoidance}
For every \textit{Action} class there is a \texttt{isPossible(\textit{World})} method that checks whether the action can or cannot be performed given a situation on the pitch (a world). Obstacle avoidance was implemented for action \textit{Move}, where the action is \textbf{not} possible if either: 
\begin{itemize}
  \item The target is out of the pitch or will hit the wall 
  \item The robot is not facing the target (with threshold $\frac{1}{18}$ radians)
  \item There is a robot between our robot and the target
  \item The target is in opponent's defense area
  \item The target is in our defense area and our teammate is in there as well 
\end{itemize}
and for rotating, where the action will not be performed if by rotating our robot would hit another robot or a wall.  

\subsection{Executor}

The executor is responsible for conveying the sequence of \textit{Actions} to each robot. Furthermore, it also updates the current beliefs about the world by using command reception feedback. Lastly, it makes sure that each instruction reaches the robot and that the robots do not get in an undefined state.

By design, whenever a new plan is received, the old one is abolished as soon as possible which, however, may not be immediately---that depends on the first \textit{Action} of the new plan and the currently executed one. Some actions (e.g. \textit{Move}) need to be ensued by the stop command, whereas other ones such as \textit{Kick} simply have to finish so that the robot does not roam around with an open grabber, for instance.

Each \textit{Action} has a corresponding \texttt{ActionHandler} to which the executor injects the necessary dependencies and then gives it control periodically by invoking the \texttt{act()} method until the handler declares that the job has been done, at which point the executor moves on to the next \textit{Action}.

All handlers inherit from the \texttt{ActionHandler} class, which among other things facilitates the solution of the common problem they all face---reliable delivery of instructions with support for separate timeouts for each robots and re-trials.
