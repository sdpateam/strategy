#!/bin/bash

# This will set the submodules to their latest commit. I know that its not using the full potential of submodules,
# but we can assume that the latest commit will always work and hence be preferable.
echo "Setting up submodules"

git submodule init

git submodule update --recursive

git submodule foreach git pull origin master