package strategy.executor;

public class SynchroniseHandler extends ActionHandler {

    @Override
    public void interruptAction() {

    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    protected void sendCommand(String identifier) {

    }

    @Override
    protected void act() {

    }
}
