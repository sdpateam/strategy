package strategy.executor;

public class IdleHandler extends ActionHandler {

    @Override
    public void interruptAction() {

    }

    @Override
    public boolean isFinished() {
        return true;
    }

    @Override
    protected void sendCommand(String identifier) {

    }

    @Override
    protected void act() {

    }
}
