package strategy.executor;

public class GrabHandler extends UninterruptibleActionHandler {

    /**
     * These strings are completely arbitrary. They're just identifiers.
     */
    private static final String REVERSE_IDENTIFIER = "reverse";
    private static final String STOP_IDENTIFIER = "stop";
    private static final String UNGRAB_IDENTIFIER = "ungrab";
    private static final String APPROACH_IDENTIFIER = "approach";
    private static final String GRAB_IDENTIFIER = "grab";

    private static final int moveSpeed = 150;

    protected String[] commandSequence = new String[] {
            REVERSE_IDENTIFIER,
            STOP_IDENTIFIER,
            UNGRAB_IDENTIFIER,
            APPROACH_IDENTIFIER,
            GRAB_IDENTIFIER,
            STOP_IDENTIFIER
    };

    protected int[] delaysBeforeCommand = new int[] {
            0,      // REVERSE
            1000,   // STOP
            0,      // UNGRAB
            1000,   // APPROACH
            2000,   // GRAB
            1000    // STOP
    };

    @Override
    public void initialize() {
        currentCommand = 0;
    }

    protected void sendReverseCommand() {
        communication.move(action.thisRobot, 0, -1, 0, moveSpeed);
    }

    protected void sendApproachCommand() {
        communication.move(action.thisRobot, 0, 1, 0, moveSpeed);
    }

    protected void sendStopCommand() {
        communication.stop(action.thisRobot);
    }

    @Override
    protected void sendCommand(String identifier) {
        switch (identifier) {
            case REVERSE_IDENTIFIER:
                sendReverseCommand();
                return;
            case STOP_IDENTIFIER:
                sendStopCommand();
                return;
            case UNGRAB_IDENTIFIER:
                sendUngrabCommand();
                return;
            case APPROACH_IDENTIFIER:
                sendApproachCommand();
                return;
            case GRAB_IDENTIFIER:
                sendGrabCommand();
        }
    }

    @Override
    protected void onCommandSent(String identifier) {
        lastCommandTime = System.currentTimeMillis();
        currentCommand++;

        if (currentCommand >= commandSequence.length) {
            isFinished = true;
        }

        if (GRAB_IDENTIFIER.equals(identifier)) {
            worldCalculator.grabbedBall(action.thisRobot);
        }
    }

    @Override
    protected void act() {
        if (enoughTimePassed(lastCommandTime, delaysBeforeCommand[currentCommand])) {
            deliverCommand(getTimeout(), commandSequence[currentCommand]);
        }
    }
}
