package strategy.executor;


public abstract class UninterruptibleActionHandler extends ActionHandler {

    protected int currentCommand = 0;

    protected long lastCommandTime;

    @Override
    public void initialize() {
        lastCommandTime = System.currentTimeMillis();
    }

    protected void sendGrabCommand() {
        communication.catchBall(action.thisRobot, 1);
    }

    protected void sendUngrabCommand() {
        System.out.println("ungrab");
        communication.catchBall(action.thisRobot, 0);
    }

    @Override
    public void interruptAction() {
        // TODO: we could technically still abort at certain stages (e.g. while we're reversing)

        // Nope
    }
}
