package strategy.executor;


import strategy.commons.Geometry;
import vision.tools.VectorGeometry;

/**
 * The name of this class (and its action) is misleading. It only supports sideways holonomic movement.
 */
public class HolonomicMoveHandler extends InterruptibleActionHandler {

    private int directionSignum;

    private static final double TOLERANCE = 1.5;

    @Override
    public void initialize() {
        directionSignum = (int) Math.signum(calculateAngleDelta());
        System.out.println("Holonomic move sideways!");
    }

    private double calculateAngleDelta() {
        return Geometry.angleTowards(
                worldCalculator.getCalculatedWorld().getRobot(action.thisRobot).location,
                action.target
        );
    }

    private int[] generateMoveCommandArguments() {
        return new int[] {directionSignum, 0, 0, 255}; // (x, y, omega, power)
    }

    @Override
    protected void sendCommand(String identifier) {
        if (STOP_IDENTIFIER.equals(identifier)) {
            System.out.println("stopping ourselves!");
            sendStopCommand();
        } else {
            communication.move(action.thisRobot, generateMoveCommandArguments());
        }
    }

    private boolean isRobotCloseEnough() {
        double distance = VectorGeometry.distance(worldCalculator.getCalculatedWorld().getRobot(action.thisRobot).location, action.target);

        double angleDelta = calculateAngleDelta();
        return distance < TOLERANCE || (int) Math.signum(angleDelta) != directionSignum;
    }

    @Override
    protected void act() {
        deliverCommand(getTimeout(), isRobotCloseEnough() ? STOP_IDENTIFIER : null);
    }
}
