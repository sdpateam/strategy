package strategy.executor;

import strategy.commons.DynamicWorld;
import vision.tools.VectorGeometry;

public class MoveHandler extends InterruptibleActionHandler {

    /**
     * How many centimeters away from the target can we be to say "meh, close enough!"?
     */
    private static final int DISTANCE_TOLERANCE = 5;

    private int[] generateMoveCommandArguments() {
        // TODO: make this eventually dynamic
        return new int[] {0, 1, 0};
    }

    @Override
    public void initialize() {

    }

    @Override
    protected void sendCommand(String identifier) {
        if (STOP_IDENTIFIER.equals(identifier)) {
            sendStopCommand();
        } else {
            //communication.move(action.thisRobot, generateMoveCommandArguments());
            communication.moveForever(action.thisRobot);
        }
    }

    private boolean isRobotCloseEnough() {
        DynamicWorld world = worldCalculator.getCalculatedWorld();

        return VectorGeometry.distance(world.getRobot(action.thisRobot).location, action.target) <= DISTANCE_TOLERANCE;
    }

    @Override
    protected void act() {
        deliverCommand(getTimeout(), isRobotCloseEnough() ? STOP_IDENTIFIER : null);
    }
}
