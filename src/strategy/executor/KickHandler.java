package strategy.executor;

public class KickHandler extends UninterruptibleActionHandler {

    /**
     * These strings are completely arbitrary. They're just identifiers.
     */
    private static final String UNGRAB_IDENTIFIER = "ungrab";
    private static final String KICK_IDENTIFIER = "kick";
    private static final String GRAB_IDENTIFIER = "grab";

    protected String[] commandSequence = new String[] {
            UNGRAB_IDENTIFIER,
            KICK_IDENTIFIER,
            GRAB_IDENTIFIER
    };

    protected int[] delaysBeforeCommand = new int[] {
            0,      // UNGRAB
            1500,   // KICK
            2000,   // GRAB
    };

    protected void sendKickCommand() {
        communication.kick(action.thisRobot);
    }

    @Override
    protected void onCommandSent(String identifier) {
        lastCommandTime = System.currentTimeMillis();

        currentCommand++;

        if (currentCommand >= commandSequence.length) {
            isFinished = true;
        }

        if (GRAB_IDENTIFIER.equals(identifier)) {
            worldCalculator.grabbedBall(action.thisRobot);
        } else if (KICK_IDENTIFIER.equals(identifier)) {
            worldCalculator.releasedBall(action.thisRobot);
        }
    }

    @Override
    protected void sendCommand(String identifier) {
        switch (identifier) {
            case UNGRAB_IDENTIFIER:
                sendUngrabCommand();
                return;
            case KICK_IDENTIFIER:
                sendKickCommand();
                return;
            case GRAB_IDENTIFIER:
                sendGrabCommand();
        }
    }

    @Override
    protected void act() {
        if (enoughTimePassed(lastCommandTime, delaysBeforeCommand[currentCommand])) {
            deliverCommand(getTimeout(), commandSequence[currentCommand]);
        }

    }
}