package strategy.executor;

import semicomms.Communication;
import semicomms.CommunicationListener;
import strategy.WorldCalculator;
import strategy.action.Action;
import vision.RobotType;
import vision.DynamicWorld;
import vision.VisionListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Part of project strategy
 */
public class Executor implements CommunicationListener, VisionListener {
    private WorldCalculator worldCalculator;
    private Communication communication;

    public HashMap<RobotType, List<Action>> plans;
    private HashMap<RobotType, ActionHandler> handlers;

    private ActionHandler idleHandler = new IdleHandler();

    private static final RobotType[] controlledRobots = {RobotType.FRIEND_1, RobotType.FRIEND_2};


    public Executor(WorldCalculator worldCalculator) {
        this.worldCalculator = worldCalculator;
        communication = Communication.getCommunication();

        initializePlans();
        initializeHandlers();
    }

    private HashMap<RobotType, List<Action>> createEmptyPlans() {
        HashMap<RobotType, List<Action>> emptyPlans = new HashMap<>();

        for (RobotType robotType : controlledRobots) {
            emptyPlans.put(robotType, new ArrayList<Action>());
        }

        return emptyPlans;
    }

    private void initializePlans() {
        plans = createEmptyPlans();
    }

    private void initializeHandlers() {
        handlers = new HashMap<>();

        for (RobotType robotType : controlledRobots) {
            handlers.put(robotType, new IdleHandler());
        }
    }

    /**
     * We can only execute the plan and make adjustments so fast. The upper bound on frequency is the rate at which
     * we get new staticWorld from the vision system. Thus we just subscribe to it as a VisionListener and use it as
     * a metronome.
     * TODO: we are assuming that by the time this gets executed, the new CalculatedWorld is already available, that
     * is that the WorldCalculator gets added as a VisionListener before the Executor. This is somewhat crappy
     * design.
     */
    @Override
    public void nextWorld(DynamicWorld dynamicWorld) {
        tick();
    }

    @Override
    public void receivedStringHandler(RobotType robotType, String signal) {
        handlers.get(robotType).receiveSignalFromRobot(signal);
    }


    /**
     * Overwrites the current plan
     *
     * @param newPlans A map of each robot to its plan
     */
    public void setNewPlans(HashMap<RobotType, List<Action>> newPlans) {
        for (Map.Entry<RobotType, List<Action>> entry : newPlans.entrySet()) {
            List<Action> newPlan = entry.getValue();

            RobotType robotType = entry.getKey();
            List<Action> currentPlan = plans.get(robotType);

            if (currentPlan == null || currentPlan.isEmpty()) { // The robot is idle
                plans.put(robotType, newPlan);
                continue;
            }

            Action currentAction = currentPlan.get(0); // That's what the robot is doing at the moment

            if (currentAction.isInterruptible()) { // Can we just tell it to do something else?
                Action nextAction = newPlan.isEmpty() ? null : newPlan.get(0);

                if (nextAction != null && currentAction.getClass().equals(nextAction.getClass())) {
                    /*  We're going to be doing the same thing, just slightly differently (e.g. rotate faster)
                        and so we don't need to tell the robot to stop. Instead, we can just overwrite the parameters.
                        So we will just get rid of the handler and will later generate a new one for the next action.
                     */
                    // TODO: should this branch also include transitions like move->rotate ?
                    handlers.put(robotType, createActionHandler(null));
                    continue;
                } else {
                    /*  this may take a little while but after it's done, the handler's isfinished() method will return
                        true, and thus the handler will get replaced by the next one from tick().
                     */
                    handlers.get(robotType).interruptAction();
                }
            }
            newPlan.add(0, currentAction);
            plans.put(robotType, newPlan);
        }
    }

    private ActionHandler createActionHandler(Action action) {
        if (action == null) {
            return idleHandler;
        }
        ActionHandler handler = action.createActionHandler();

        handler
                .setAction(action)
                .setWorldCalculator(worldCalculator)
                .setCommunication(communication)
                .initialize();

        return handler;
    }

    private boolean isRobotSynchronizing(RobotType robotType) {
        return handlers.get(robotType) instanceof SynchroniseHandler;
    }

    private boolean isRobotIdle(RobotType robotType) {
        return handlers.get(robotType) instanceof IdleHandler;
    }

    private void moveToNextAction(RobotType robotType) {
        List<Action> plan = plans.get(robotType);

        if (plan != null && !plan.isEmpty()) {
            if (!isRobotIdle(robotType)) {
                plan.remove(0);
            }
            handlers.put(robotType, createActionHandler(plan.isEmpty() ? null : plan.get(0)));
        } else {
            handlers.put(robotType, createActionHandler(null));
        }
    }

    private void tick() {
        boolean areRobotsSynchronized = true;

        for (Map.Entry<RobotType, ActionHandler> entry : handlers.entrySet()) {
            RobotType robotType = entry.getKey();
            ActionHandler handler = entry.getValue();

            if (handler.isFinished()) {
                moveToNextAction(robotType);
            }
            areRobotsSynchronized &= isRobotSynchronizing(robotType);
        }

        for (RobotType robotType: handlers.keySet()) {
            if (areRobotsSynchronized) {
                // All robots have now reached the synchronize action and so we can move on with the plans
                moveToNextAction(robotType);
            }
            handlers.get(robotType).tick();
        }
    }

    public void abortPlans() {
        setNewPlans(createEmptyPlans());
    }
}
