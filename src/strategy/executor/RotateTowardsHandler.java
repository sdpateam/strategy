package strategy.executor;

import strategy.commons.Geometry;
import vision.tools.DirectedPoint;

public class RotateTowardsHandler extends InterruptibleActionHandler {

    /**
     * How many degrees away from the target can we be to say "meh, close enough!"?
     */
    private static final double ROTATION_TOLERANCE = Math.PI / 36;

    private static final String ROTATE_IDENTIFIER = "rotate";

    private boolean commandSent = false;

    private double angleDelta;

    /**
     * 1 and 0 indicate clockwise and anti-clockwise rotation respectively
     */
    private double directionSignum;

    @Override
    public void initialize() {
        computeAngleDelta();

        directionSignum = Math.signum(angleDelta);
    }

    private void computeAngleDelta() {
        angleDelta = Geometry.angleTowards(
                worldCalculator.getCalculatedWorld().getRobot(action.thisRobot).location,
                action.target
        );
    }

    @Override
    protected void sendCommand(String identifier) {
        if (STOP_IDENTIFIER.equals(identifier)) {
            sendStopCommand();
        } else {
            communication.rotateForever(action.thisRobot, (int) directionSignum);
        }
    }

    @Override
    public void onCommandSent(String identifier) {
        super.onCommandSent(identifier);

        if (ROTATE_IDENTIFIER.equals(identifier)) {
            commandSent = true;
        }
    }

    private boolean isRobotCloseEnough() {
        return ROTATION_TOLERANCE > Math.abs(angleDelta);
    }

    @Override
    protected void act() {
        computeAngleDelta();
        System.out.println(angleDelta);

        if (isRobotCloseEnough()) {
            deliverCommand(getTimeout(), STOP_IDENTIFIER);
            System.out.println("");
        } else {
//            if (Math.signum(angleDelta) != directionSignum) { // We overshot!
//                commandSent = false;
//                directionSignum = -directionSignum;
//            }

            if (!commandSent) {
                deliverCommand(getTimeout(), ROTATE_IDENTIFIER);
            }
        }
    }
}