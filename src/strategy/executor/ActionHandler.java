package strategy.executor;

import semicomms.Communication;
import strategy.WorldCalculator;
import strategy.action.Action;
import vision.RobotType;

public abstract class ActionHandler {

    protected WorldCalculator worldCalculator;
    protected Communication communication;
    protected Action action;

    protected boolean isFinished = false;

    private boolean waitingForDelivery = false;
    private String commandToDeliver = null;
    private long lastDeliveryAttempt;
    private int deliveryTimeout;

    /**
     * This is the number of milliseconds we will wait before trying to send a command again
     */
    protected static final int ROBOT_1_TIMEOUT = 500;
    protected static final int ROBOT_2_TIMEOUT = 500;


    public ActionHandler setWorldCalculator(WorldCalculator worldCalculator) {
        this.worldCalculator = worldCalculator;
        return this;
    }

    public ActionHandler setCommunication(Communication communication) {
        this.communication = communication;
        return this;
    }

    public ActionHandler setAction(Action action) {
        this.action = action;
        return this;
    }

    protected int getTimeout() {
        return action.thisRobot == RobotType.FRIEND_1 ? ROBOT_1_TIMEOUT : ROBOT_2_TIMEOUT;
    }

    protected void deliverCommand(int timeout, String command) {
        waitingForDelivery = true;
        deliveryTimeout = timeout;
        lastDeliveryAttempt = System.currentTimeMillis();
        sendCommand(commandToDeliver = command);
    }
    /**
     * The descendants may override this if they feel like it
     */
    protected void onCommandSent(String identifier) {

    }

    public void initialize() {

    }

    protected boolean enoughTimePassed(long since, long delta) {
        return (System.currentTimeMillis() - since) >= delta;
    }

    public void receiveSignalFromRobot(String signal) {
        if (signal.equals("A")) {
            waitingForDelivery = false;
            onCommandSent(commandToDeliver);
        }
    }

    public void tick() {
        if (waitingForDelivery) {
            if (enoughTimePassed(lastDeliveryAttempt, deliveryTimeout)) {
                sendCommand(commandToDeliver);
            }
        } else if (!isFinished()) {
            act();
        }
    }



    protected abstract void sendCommand(String identifier);

    abstract void interruptAction();

    public boolean isFinished() {
        return isFinished;
    }

    /**
     * This method is very much like tick() for the descendants, except it doesn't get called when the handler is
     * waiting for a command to be delivered.
     */
    protected abstract void act();
}
