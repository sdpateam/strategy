package strategy.executor;

public abstract class InterruptibleActionHandler extends ActionHandler {

    protected final String STOP_IDENTIFIER = "stop";

    @Override
    public void interruptAction() {
        deliverCommand(getTimeout(), STOP_IDENTIFIER);
    }

    @Override
    public void onCommandSent(String identifier) {
        if (STOP_IDENTIFIER.equals(identifier)) {
            isFinished = true;
        }
    }

    protected void sendStopCommand() {
        communication.stop(action.thisRobot);
    }
}
