package strategy.commons;

import vision.Ball;
import vision.Robot;
import vision.RobotType;
import vision.tools.DirectedPoint;
import vision.tools.VectorGeometry;

import java.util.ArrayList;
import java.util.List;

import static strategy.commons.Constants.*;

public class DynamicWorld extends vision.DynamicWorld {

    public final int granularity;

    public List<RobotType> robotsOnPitch;
    public List<RobotType> activeRobots;
    public RobotType robotWithBall;

    public boolean attackingPositive;

    // Pitch dimensions. All of these are the distance from 0!!!
    public final int PITCH_HALF_LENGTH;
    public final int PITCH_HALF_WIDTH;

    public final VectorGeometry FRIEND_GOAL_POST_BOTTOM;
    public final VectorGeometry FRIEND_GOAL_POST_TOP;
    public final VectorGeometry FRIEND_DEF_BOTTOM_RIGHT;
    public final VectorGeometry FRIEND_DEF_TOP_RIGHT;
    public final VectorGeometry FRIEND_DEF_BOTTOM_LEFT;
    public final VectorGeometry FRIEND_DEF_TOP_LEFT;

    public final VectorGeometry FOE_GOAL_POST_BOTTOM;
    public final VectorGeometry FOE_GOAL_POST_TOP;
    public final VectorGeometry FOE_DEF_BOTTOM_RIGHT;
    public final VectorGeometry FOE_DEF_TOP_RIGHT;
    public final VectorGeometry FOE_DEF_BOTTOM_LEFT;
    public final VectorGeometry FOE_DEF_TOP_LEFT;

    /**
     * Creates a simplified version of the world where dimensions are in squares of higher granularity than 1x1 cm.
     * Also stores information about which robots are on the pitch and which has ball (based on both Vision and
     * Executor)
     * Also stores dimensions of the pitch.
     *  @param visionWorld new frame from the vision component
     * @param oldWorld
     * @param granularity the length of a side of the square the world is sliced into
     */
    public DynamicWorld(vision.DynamicWorld visionWorld, DynamicWorld oldWorld, int granularity, boolean attackingPositive, List<RobotType> activeRobots) {
        super(); // Initializes robots hashmap
        this.robotsOnPitch = new ArrayList<>();
        this.activeRobots = activeRobots;

        this.granularity = granularity;
        this.attackingPositive = attackingPositive;

        // Recalculates position of ball into square units
        if (oldWorld!= null) this.setBall(oldWorld.getBall());

        if (visionWorld.getBall() != null) {
            Ball ballFromVision = visionWorld.getBall().clone();
            ballFromVision.location.x = toSquares((int) Math.round(ballFromVision.location.x));
            ballFromVision.location.y = toSquares((int) Math.round(ballFromVision.location.y));
            this.setBall(ballFromVision);
        }

        // Recalculates positions of robots into square units
        for (RobotType robot : RobotType.values()) {
            if (oldWorld != null) {
                Robot oldRobot = oldWorld.getRobot(robot);
                if (oldRobot != null) {
                    this.setRobot(oldRobot.clone());
                }
            }

            if (visionWorld.getRobot(robot) != null) {
                Robot robotFromVision = visionWorld.getRobot(robot).clone();

                robotFromVision.location = new DirectedPoint(
                        toSquares((int) Math.round(robotFromVision.location.x)),
                        toSquares((int) Math.round(robotFromVision.location.y)),
                        // Makes sure that the direction is in range (-dpi,pi)
                        Geometry.normaliseAngle(robotFromVision.location.direction));
                this.setRobot(robotFromVision);

                // Also adds info about which robots are on the pitch
                robotsOnPitch.add(robot);
            }
        }

        // Bundles pitch dimensions & defense area into the object
        // TODO: we can (hopefully) get this dynamically from the vision
        this.PITCH_HALF_LENGTH = PITCH_LENGTH / 2;
        this.PITCH_HALF_WIDTH = PITCH_WIDTH / 2;


        // This ugliness is here so that I don't have to write out the FOE_DEF_ shit two times
        int offset = 0;
        int sign = -1;
        if (attackingPositive) {
            offset = PITCH_HALF_LENGTH + DEFENSE_AREA_FROM_CENTER;
            sign = 1;
        }

        // The pitch represented as a set of points derived from the Constants.java file
        this.FOE_DEF_TOP_RIGHT = new VectorGeometry(toSquares(-DEFENSE_AREA_FROM_CENTER + offset), toSquares(DEFENSE_AREA_TOP));
        this.FOE_DEF_BOTTOM_RIGHT = new VectorGeometry(toSquares(-DEFENSE_AREA_FROM_CENTER + offset), toSquares(DEFENSE_AREA_BOTTOM));
        this.FOE_DEF_BOTTOM_LEFT = new VectorGeometry(toSquares(-PITCH_LENGTH/2 + offset), toSquares(DEFENSE_AREA_BOTTOM));
        this.FOE_DEF_TOP_LEFT = new VectorGeometry(toSquares(-PITCH_LENGTH/2 + offset), toSquares(DEFENSE_AREA_TOP));
        this.FOE_GOAL_POST_TOP = new VectorGeometry(toSquares(sign * PITCH_LENGTH/2), toSquares(GOAL_POST_TOP));
        this.FOE_GOAL_POST_BOTTOM = new VectorGeometry(toSquares(sign * PITCH_WIDTH/2), toSquares(GOAL_POST_BOTTOM));

        this.FRIEND_DEF_TOP_RIGHT = new VectorGeometry(toSquares(PITCH_LENGTH/2 - offset), toSquares(DEFENSE_AREA_TOP));
        this.FRIEND_DEF_BOTTOM_RIGHT = new VectorGeometry(toSquares(PITCH_LENGTH/2 - offset), toSquares(DEFENSE_AREA_BOTTOM));
        this.FRIEND_DEF_TOP_LEFT = new VectorGeometry(toSquares(DEFENSE_AREA_FROM_CENTER - offset), toSquares(DEFENSE_AREA_TOP));
        this.FRIEND_DEF_BOTTOM_LEFT = new VectorGeometry(toSquares(DEFENSE_AREA_FROM_CENTER -offset), toSquares(DEFENSE_AREA_BOTTOM));
        this.FRIEND_GOAL_POST_TOP = new VectorGeometry(toSquares(-sign * PITCH_LENGTH/2), toSquares(GOAL_POST_TOP));
        this.FRIEND_GOAL_POST_BOTTOM = new VectorGeometry(toSquares(-sign * PITCH_LENGTH/2), toSquares(GOAL_POST_TOP));


        // Determines who has the ball
        // TODO: Simon said the vision is already able to do this.
        this.setProbableBallHolder(visionWorld.getProbableBallHolder());
    }

    /**
     * Clones the other CalculateWorld object
     *
     * @param oldWorld
     */
    public DynamicWorld(DynamicWorld oldWorld) {
        this.granularity = oldWorld.granularity;

        this.setBall(oldWorld.getBall().clone());

        for (Robot robot : oldWorld.getRobots()) {
            this.setRobot(robot.clone());
        }
        this.robotsOnPitch = oldWorld.robotsOnPitch;

        this.PITCH_HALF_LENGTH = oldWorld.PITCH_HALF_LENGTH;
        this.PITCH_HALF_WIDTH = oldWorld.PITCH_HALF_WIDTH;

        this.FRIEND_GOAL_POST_BOTTOM = oldWorld.FRIEND_GOAL_POST_BOTTOM;
        this.FRIEND_GOAL_POST_TOP = oldWorld.FRIEND_GOAL_POST_TOP;
        this.FRIEND_DEF_BOTTOM_RIGHT = oldWorld.FRIEND_DEF_BOTTOM_RIGHT;
        this.FRIEND_DEF_TOP_RIGHT = oldWorld.FRIEND_DEF_TOP_RIGHT;
        this.FRIEND_DEF_TOP_LEFT = oldWorld.FRIEND_DEF_TOP_LEFT;
        this.FRIEND_DEF_BOTTOM_LEFT= oldWorld.FRIEND_DEF_BOTTOM_LEFT;

        this.FOE_GOAL_POST_BOTTOM = oldWorld.FOE_GOAL_POST_BOTTOM;
        this.FOE_GOAL_POST_TOP = oldWorld.FOE_GOAL_POST_TOP;
        this.FOE_DEF_BOTTOM_RIGHT = oldWorld.FOE_DEF_BOTTOM_RIGHT;
        this.FOE_DEF_TOP_RIGHT = oldWorld.FOE_DEF_TOP_RIGHT;
        this.FOE_DEF_TOP_LEFT = oldWorld.FOE_DEF_TOP_LEFT;
        this.FOE_DEF_BOTTOM_LEFT= oldWorld.FOE_DEF_BOTTOM_LEFT;
    }

    /**
     * Computes the square value with offset, since the first square is centered at (0,0)
     *
     * @param value integer value
     * @return value in square unit system
     */
    private int toSquares(int value) {
        double offset = this.granularity / 2.0;
        double sqValue;

        if (value > offset) {
            sqValue = (value + offset) / this.granularity;
        } else if (value < -offset) {
            sqValue = (value - offset) / this.granularity;
        } else {
            sqValue = 0;
        }

        // Returns the value rounded to the nearest integer
        return (int) (0.5 + sqValue);
    }
}
