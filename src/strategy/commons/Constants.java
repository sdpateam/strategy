package strategy.commons;

import vision.tools.VectorGeometry;

/**
 * Part of project strategy.strategy
 *
 * Created by Jano Horvath on 27/01/16.
 */
public class Constants {

    public static final double BALL_RADIUS = 5.6;
    public static final double MAX_ROBOT_SIZE = 20;

    /**
     * Pitch dimensions.
     * (0,0) is in the CENTER of the pitch
     * pitches in both rooms have the same dimensions
     */
    public static final int PITCH_LENGTH = 145 * 2;
    public static final int PITCH_WIDTH = 105 * 2;

    public static final int GOAL_POST_BOTTOM = -30;
    public static final int GOAL_POST_TOP = 30;
    public static final int GOAL_DEPTH = 10;

    public static final int DEFENSE_AREA_TOP = 60;
    public static final int DEFENSE_AREA_BOTTOM = -60;
    public static final int DEFENSE_AREA_FROM_CENTER = 75;

    public static final int KICK_SHOOT_POWER = 100;
    public static final int KICK_PASS_POWER = 50;
    public static final int MOVE_FULL_POWER = 100;

    public static final VectorGeometry CENTER_OF_GOAL = new VectorGeometry(PITCH_LENGTH, 0);
}
