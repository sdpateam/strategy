package strategy.commons;

import vision.tools.DirectedPoint;
import vision.tools.Point;
import vision.tools.VectorGeometry;

public class Geometry {
    public static double distance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(squareDistance(x1, y1, x2, y2));
    }

    public static double distance(VectorGeometry p1, VectorGeometry p2) {
        return distance(p1.x, p1.y, p2.x, p2.y);
    }

    public static double squareDistance(double x1, double y1, double x2, double y2) {
        double x = x1 - x2;
        double y = y1 - y2;
        double res = x * x + y * y;
        return res;
    }

    // Computes the euclidean distance from a line which is defined by points l1 and l2
    //  as seen in https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_an_equation
    public static double distanceFromLine(VectorGeometry l1, VectorGeometry l2, VectorGeometry target) {
        double numerator = Math.abs((l2.y-l1.y)*target.x - (l2.x - l1.x)*target.y + l2.x*l1.y - l2.y*l1.x);
        double denominator = Math.sqrt(Math.pow(l2.y - l1.y, 2) + Math.pow(l2.x - l1.x, 2));
        return numerator / denominator;
    }

    public static double distanceFromLine(DirectedPoint p, VectorGeometry target) {
        VectorGeometry l2 = new VectorGeometry((int) (p.x + Math.floor(2 * Math.cos(p.direction))),
                             (int) (p.y + Math.floor(2 * Math.sin(p.direction))));

        return distanceFromLine(p, l2, target);
    }

    public static double squareLength(double x, double y) {
        double res = squareDistance(0, 0, x, y);
        return res;
    }

    public static double length(double x, double y) {
        double res = Math.sqrt(squareLength(x, y));
        return res;
    }

    // Computes a signed angle normalised to (-pi,pi) between two vectors using polar coordinates.
    public static double angle(double x1, double y1, double x2, double y2) {
        return normaliseAngle(Math.atan2(y2, x2) - Math.atan2(y1, x1));
    }

    // Computes a signed angle normalised to (-pi,pi) between two vectors
    public static double angle(VectorGeometry p1, VectorGeometry p2) {
        return angle(p1.x, p1.y, p2.x, p2.y);
    }

    // Computes a signed angle normalised to (-pi,pi) between the vector and a (1,0) unit vector
    public static double angle(double x, double y) {
        return angle(1, 0, x, y);
    }

    /**
     * Computes the difference between the angle in p1 and the angle between p1 and p2.
     * @param p1 e.g. position of the robot
     * @param p2 e.g. position of the ball
     * @return angle in radians e.g. the robot should rotate to face the ball
     */
    public static double angleTowards(DirectedPoint p1, VectorGeometry p2) {
        double a1 = p1.direction;
        double angle = Math.atan2(p2.y-p1.y , p2.x-p1.x);
        angle = (angle - a1) - 2*Math.PI * (int)((angle-a1)/(2*Math.PI));

        if (Double.isNaN(angle)){
            return Math.PI/2.0;
        }
        return normaliseAngle(angle);
    }

    /**
     * Takes any angle in radians and returns its equivalent in the range (-pi, pi)
     * @param angle in radians
     * @return equivalent angle in range (-pi,pi)
     */
    public static double normaliseAngle(double angle) {
        // Limits the angle to (-2pi, 2pi)
        angle = angle % (2*Math.PI);

        // Then translates everything to range (-pi,pi)
        // NB: due to rounding of doubles, -pi gets translated to pi and vice versa.
        if (angle < - Math.PI) {
            return 2*Math.PI + angle;
        }

        if (angle > Math.PI) {
            return angle - 2*Math.PI;
        }

        return angle;
    }

    public static VectorGeometry midpoint(VectorGeometry p1, VectorGeometry p2) {
        return new VectorGeometry(p1.x + p2.x / 2, p1.y + p2.y / 2);
    }

    public static boolean inRegion(VectorGeometry p, VectorGeometry regionTopLeft, VectorGeometry regionBottomRight) {
        return p.x > regionTopLeft.x
                && p.x < regionBottomRight.x
                && p.y > regionBottomRight.y
                && p.y < regionTopLeft.y;
    }

    public static void main(String[] args) {
        System.out.println(angle(0, -1));
    }
}
