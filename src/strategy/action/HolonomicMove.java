package strategy.action;

import strategy.commons.DynamicWorld;
import strategy.commons.Geometry;
import strategy.executor.HolonomicMoveHandler;
import vision.RobotType;
import strategy.executor.ActionHandler;
import vision.tools.DirectedPoint;
import vision.tools.VectorGeometry;

/**
 * Created by Mark on 12/03/2016.
 */
public class HolonomicMove extends Action {

    public HolonomicMove(DynamicWorld currentState, RobotType thisRobot, VectorGeometry target) {
        super(ActionType.HOLONOMIC_MOVE, currentState, thisRobot, target);
    }

    @Override
    public boolean isPossible(DynamicWorld world) {
        DirectedPoint current = world.getRobot(thisRobot).location;
        if (current  == null) {
            return false;
        }
        if (thisRobot != RobotType.FRIEND_1) {
            return false;
        }
        if(target == null) {
            return false;
        }
        // TODO check if we're FRIEND_1 as well as actual possibility
        DirectedPoint friend;
        for(RobotType robot:world.robotsOnPitch) {
            if (robot != thisRobot) {
                //System.out.println(rob.x + " " + rob.y + "\n");
                DirectedPoint rob = world.getRobot(robot).location;
                if (rob.x > current.x  - 15/world.granularity &&
                        rob.x < current.x + 15/world.granularity &&
                        rob.y > Math.min(current.y, target.y) - 15/world.granularity &&
                        rob.y < Math.max(current.y, target.y) + 15/world.granularity) {
                    System.out.println("Robot in the way");
                    return false;
                }
            }
        }

        return true; // TODO false because we can't execute it yet
    }

    @Override
    public boolean deviated(DynamicWorld latestWorld) {
        double distFromTrajectory = Geometry.distanceFromLine(
                world.getRobot(thisRobot).location, target, latestWorld.getRobot(thisRobot).location
        );
        return distFromTrajectory > 20/latestWorld.granularity; // 20 = size of robot
    }

    @Override
    public DynamicWorld getFutureWorld() {
        DirectedPoint newPos = new DirectedPoint(target.x, target.y, world.getRobot(thisRobot).location.direction);
        DynamicWorld future = new DynamicWorld(world);
        future.getRobot(this.thisRobot).location = newPos;

        return future;
    }

    @Override
    public double cost() {
        return Geometry.distance(world.getRobot(thisRobot).location, target);
    }

    @Override
    public boolean isInterruptible() {
        return true;
    }

    @Override
    public ActionHandler createActionHandler() {
        return new HolonomicMoveHandler();
    }
}


