package strategy.action;

/**
 * Created by s1309061 on 10/03/16.
 */
public enum ActionType {

    // Real Actions
    GRAB,
    KICK,

    MOVE_FORWARDS_OR_BACKWARDS,
    HOLONOMIC_MOVE,
    HOLONOMIC_MOVE_AND_ROTATE,

    ROTATE_TOWARDS,

    SYNCHRONISE

}
