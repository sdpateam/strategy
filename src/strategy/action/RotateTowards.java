package strategy.action;

import strategy.commons.DynamicWorld;
import strategy.commons.Geometry;
import vision.RobotType;
import strategy.executor.ActionHandler;
import strategy.executor.RotateTowardsHandler;
import vision.tools.DirectedPoint;
import vision.tools.VectorGeometry;

/**
 * Part of project strategy
 * <p/>
 * Created by Jano Horvath on 17/02/16.
 */
public class RotateTowards extends Action {

    private double angle; //in Radians

    /**
     * Rotates so that it faces the target
     *
     * @param currentState
     * @param thisRobot
     * @param target
     */
    public RotateTowards(DynamicWorld currentState, RobotType thisRobot, VectorGeometry target) {
        super(ActionType.ROTATE_TOWARDS, currentState, thisRobot, target);

        DirectedPoint current = currentState.getRobot(thisRobot).location;

        angle = Geometry.angleTowards(current, target);
    }

    @Override
    public boolean isPossible(DynamicWorld world) {
        return true;
        // TODO: check for nearby robots and walls
//        DirectedPoint foe1 = world.getRobot(RobotType.FOE_1);
//        DirectedPoint foe2 = world.getRobot(RobotType.FOE_2);
//        DirectedPoint friend = world.getRobot(RobotType.FRIEND_2);
//        DirectedPoint current = world.getRobot(thisRobot);
//        if (Geometry.distance(current.x, current.y, foe1.x, foe1.y) < 25 || Geometry.distance(current.x, current.y, foe1.x, foe1.y) < 25) {
//            System.out.println("Risky to rotate! You might hit another robot!");
//            return false;
//        }
//        // Todo: check for nearby wall
//        return true;
    }

    @Override
    public boolean deviated(DynamicWorld latestWorld) {
        // See rationale in RotateDegrees
        return false;
    }

    @Override
    public DynamicWorld getFutureWorld() {
        DirectedPoint oldPos = world.getRobot(this.thisRobot).location;
        DirectedPoint newPos = new DirectedPoint(oldPos.x, oldPos.y, oldPos.direction + angle);

        DynamicWorld future = new DynamicWorld(world);
        future.getRobot(this.thisRobot).location = newPos;
        return future;
    }

    @Override
    public double cost() {
        return angle;
    }

    @Override
    public boolean isInterruptible() {
        return true;
    }

    @Override
    public ActionHandler createActionHandler() {
        return new RotateTowardsHandler();
    }
}
