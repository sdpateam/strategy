package strategy.action;

import strategy.commons.DynamicWorld;
import strategy.commons.Geometry;
import vision.RobotType;
import strategy.executor.ActionHandler;
import vision.tools.DirectedPoint;

/**
 * Created by Mark on 12/03/2016.
 */
public class HolonomicMoveAndRotate extends Action {
    private DirectedPoint target;

    public HolonomicMoveAndRotate(DynamicWorld currentState, RobotType thisRobot, DirectedPoint target) {
        super(ActionType.HOLONOMIC_MOVE_AND_ROTATE, currentState, thisRobot, target);
        this.target = target;
    }

    @Override
    public boolean isPossible(DynamicWorld world) {
        // TODO: it's just false because we can't properly execute it yet! :D
        return false;
    }

    @Override
    public boolean deviated(DynamicWorld latestWorld) {
        // TODO
        return false;
    }

    @Override
    public DynamicWorld getFutureWorld() {
        DirectedPoint newPos   = new DirectedPoint(target.x, target.y, target.direction);
        DynamicWorld future = new DynamicWorld(world);
        future.getRobot(this.thisRobot).location = newPos;

        return future;
    }

    @Override
    public double cost() {
        return Geometry.distance(world.getRobot(thisRobot).location, target);
    }

    @Override
    public boolean isInterruptible() {
        return true;
    }

    @Override
    public ActionHandler createActionHandler() {
        // TODO?
        return null;
    }
}



