package strategy.action;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import strategy.executor.ActionHandler;
import strategy.executor.GrabHandler;
import vision.tools.VectorGeometry;

/**
 * Part of project strategy
 * <p/>
 * Created by Jano Horvath on 18/02/16.
 */
public class Grab extends Action {
    public Grab(DynamicWorld currentState, RobotType thisRobot, VectorGeometry target) {
        super(ActionType.GRAB, currentState, thisRobot, target);
    }

    @Override
    public boolean isPossible(DynamicWorld world) {
        // TODO: should it be possible to grab when the ball is not there?
        return true;
    }

    @Override
    public boolean deviated(DynamicWorld latestWorld) {
        // Never deviates.
        return false;
    }

    @Override
    public DynamicWorld getFutureWorld() {
        // TODO: whoHasBall should be updated.
        return world;
    }

    @Override
    public double cost() {
        return 1;
    }

    @Override
    public boolean isInterruptible() {
        return false;
    }

    @Override
    public ActionHandler createActionHandler() {
        return new GrabHandler();
    }
}
