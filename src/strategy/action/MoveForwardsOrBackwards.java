package strategy.action;


import strategy.commons.DynamicWorld;
import strategy.commons.Geometry;
import vision.RobotType;
import strategy.executor.ActionHandler;
import strategy.executor.MoveHandler;
import vision.tools.DirectedPoint;
import vision.tools.Point;
import vision.tools.VectorGeometry;

/**
 * Created by s1309061 on 10/03/16.
 */
public class MoveForwardsOrBackwards extends Action {

    private VectorGeometry target;
    private int robotLength = 20/world.granularity;
    private int robotDiagonal = (int)(robotLength*Math.sqrt(2)/2);
    //half length of our robot + max half diagonal length of any other (to be modified if vision not accurate enough)
    private int radius = 25/world.granularity;//(int)(robotLength/2 + robotDiagonal/2 );
    /**
     * Assuming the target is in front or behind of the robot, it moves to that point
     */

    public MoveForwardsOrBackwards(DynamicWorld currentState, RobotType thisRobot, VectorGeometry target) {
        super(ActionType.MOVE_FORWARDS_OR_BACKWARDS, currentState, thisRobot, target);
        this.target = target;
    }

    private boolean rectangleContains(VectorGeometry[] corners, int x, int y) {
        double sum = 0;
        for (int i = 0; i < 3; i++) {
            sum += Math.abs((x - corners[i].x) * (corners[i + 1].y - y) -
                    (x - corners[i + 1].x) * (corners[i].y - y)) * 0.5;
//            System.out.println(sum + " ");
        }
        sum += Math.abs((x - corners[3].x) * (corners[0].y - y) - (x - corners[0].x) * (corners[3].y - y)) * 0.5;


        double area = Math.sqrt(((corners[0].x - corners[1].x) * (corners[0].x - corners[1].x)
                + (corners[0].y - corners[1].y) * (corners[0].y - corners[1].y)))
                *
                Math.sqrt(((corners[2].x - corners[1].x) * (corners[2].x - corners[1].x)
                        + (corners[2].x - corners[1].x) * (corners[2].y - corners[1].y)));
        // 50 threshhold
//        System.out.println("the area " + area + " " + sum + "\n");
        return Math.abs(area - sum) < 20/world.granularity;
    }
    // rotate coordinates having our robot as centre
    private VectorGeometry rotateCoordinates(VectorGeometry centre, VectorGeometry p, double direction) {
        VectorGeometry updateP = new VectorGeometry(0,0);
        updateP.x = (int)(centre.x + (p.x-centre.x)*Math.cos(direction) + (p.y-centre.y)*Math.sin(direction));
        updateP.y = (int)(centre.y - (p.x-centre.x)*Math.sin(direction) + (p.y-centre.y)*Math.cos(direction));
        return updateP;
    }

    @Override
    public boolean isPossible(DynamicWorld world) {

        int granularity = world.granularity;
        int length = world.PITCH_HALF_LENGTH;
        int width = world.PITCH_HALF_WIDTH;
        // Don't crash if the target (usually the ball) in not seen
        if(target == null)
            return false;
        // Check if the robot will go out of the pitch/hit the walls
        if (target.x < robotDiagonal - length
                || target.x > length - robotDiagonal
                || target.y < robotDiagonal - width
                || target.y > width - robotDiagonal) { // 15cm is the distance from centre to furthest edge
            //System.out.print(target.x +" " + target.y);
            System.out.print("out of the field" );
            return false;
        }

        DirectedPoint current = world.getRobot(thisRobot).location;
        System.out.println("target: " + target.x + " " + target.y );
        // If the robot is not on pitch don't execute
        if(current == null) {
            System.out.print("robot not on field\n" );
            return false;
        }
        double direction = current.direction;

        //If the robot is not directed to the target don't execute
        // TODO: Discuss what error should we use ( currently 10 degress)
        // TODO: The vision doesn't report robot's direction well enough for this to be reliable
        /*double angle = Math.atan2(target.y - current.y, target.x - current.x) ;
        if( angle<0) {
            angle = Math.PI + angle;
        }

        if(Math.abs(angle-Math.PI) < Math.PI/18) {
            direction = current.direction + angle;
        }
        double angle = Geometry.angle(current.x, current.y, target.x, target.y) - current.direction;
        System.out.println("Angle between " + angle );
        if(!(Math.abs(angle) > Math.PI/18.0 || Math.abs(Math.abs(angle)-Math.PI) < Math.PI/18.0)) {
            System.out.print("not good angle"+ current.direction + " " + current.x + " " + current.y + " " + target.x + " " + target.y );
            return false;
        }*/



        // Corners of the rectangle another robot should not be in, in order to move to the desired target
        VectorGeometry[] corners = new VectorGeometry[4];
        // Compute robots direction (assuming it's facing the ball)


        //VectorGeometry updateCurrent = rotateCoordinates(new VectorGeometry(current.x, current.y), direction);
        VectorGeometry updateCurrent = new VectorGeometry(current.x, current.y);
        //System.out.println("Robot: " + current.x + " " + current.y + "\n");
        VectorGeometry updateTarget = rotateCoordinates(updateCurrent, (new VectorGeometry(target.x, target.y)), direction);
        corners[0] = new VectorGeometry(updateCurrent.x , updateCurrent.y + 20/granularity);
        corners[1] = new VectorGeometry(updateCurrent.x , updateCurrent.y - 20/granularity);
        corners[2] = new VectorGeometry(updateTarget.x , updateTarget.y - 20/granularity);
        corners[3] = new VectorGeometry(updateTarget.x , updateTarget.y + 20/granularity);
        //System.out.print("angle:" + direction + "\n");
        //for(int i=0;i<4;i++)
          //  System.out.print(corners[i].x + " "+ corners[i].y + "\n")
        // Check if there is another robot in the way to the target
        DirectedPoint friend = null;
        //System.out.println("Where the other robots are: \n");
        for(RobotType robot:world.robotsOnPitch) {
            if (robot != thisRobot) {
                if(robot == RobotType.FRIEND_1)
                    friend = world.getRobot(robot).location;
                if(robot == RobotType.FRIEND_2)
                    friend = world.getRobot(robot).location;
                VectorGeometry rob = rotateCoordinates(
                        updateCurrent,
                        new VectorGeometry(world.getRobot(robot).location.x, world.getRobot(robot).location.y), direction
                );
                //System.out.println(rob.x + " " + rob.y + "\n");
                if (rob.y > updateCurrent.y  - 15/granularity &&
                        rob.y < updateTarget.y + 15/granularity &&
                        rob.x > Math.min(updateCurrent.x, updateTarget.x) - 15/granularity &&
                        rob.x < Math.max(updateCurrent.x, updateTarget.x) + 15/granularity) {
                    System.out.println("Robot in the way");
                    return false;
                }
            }
        }

        // Check if target is in foe's defence zone
        VectorGeometry[] foeDefence = new VectorGeometry[4];
        foeDefence[0] = world.FOE_DEF_TOP_RIGHT;
        foeDefence[1] = world.FOE_DEF_BOTTOM_RIGHT;
        foeDefence[2] = world.FOE_DEF_BOTTOM_LEFT;
        foeDefence[3] = world.FOE_DEF_TOP_LEFT;
        //System.out.println("FOE" + "\n");
        //for(int i=0;i<4;i++)
          //  System.out.print(foeDefence[i].x + " "+ foeDefence[i].y + "\n");
        if(rectangleContains(foeDefence, (int) target.x, (int) target.y)) {
            System.out.println("Ball in foe's defence");
            return false;
        }

        // Check if friend is in defence zone and don't execute is the target is there as well
        VectorGeometry[] friendDefence = new VectorGeometry[4];
        friendDefence[0] = world.FRIEND_DEF_TOP_RIGHT;
        friendDefence[1] = world.FRIEND_DEF_BOTTOM_RIGHT;
        friendDefence[2] = world.FRIEND_DEF_BOTTOM_LEFT;
        friendDefence[3] = world.FRIEND_DEF_TOP_LEFT;
        //System.out.println("Friend");
        //for(int i=0;i<4;i++)
          //  System.out.print(friendDefence[i].x + " "+ friendDefence[i].y + "\n");
        if(friend != null)
            if(rectangleContains(friendDefence, (int) friend.x, (int) friend.y) &&
                    rectangleContains(friendDefence, (int) target.x, (int) target.y)) {
                System.out.print("Friend in our defence. Can't go there\n");
                return false;
            }

        // If all other conditions are false move
        return true;
    }

    @Override
    public boolean deviated(DynamicWorld latestWorld) {
        double distFromTrajectory = Geometry.distanceFromLine(
                world.getRobot(thisRobot).location, target, latestWorld.getRobot(thisRobot).location
        );
        return distFromTrajectory > 2 * radius;
    }

    @Override
    public DynamicWorld getFutureWorld() {
        DirectedPoint newPos = new DirectedPoint(target.x, target.y, world.getRobot(thisRobot).location.direction);

        DynamicWorld future = new DynamicWorld(world);
        future.getRobot(this.thisRobot).location = newPos;
        return future;
    }

    @Override
    public double cost() {
        return 1;//Geometry.distance(world.getRobot(thisRobot), target);
    }

    @Override
    public boolean isInterruptible() {
        return true;
    }


    @Override
    public ActionHandler createActionHandler() {
        return new MoveHandler();
    }
}

