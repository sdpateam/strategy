package strategy.action;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import strategy.executor.ActionHandler;
import vision.tools.VectorGeometry;

/**
 * Part of project strategy.strategy
 * <p/>
 * Created by Jano Horvath on 27/01/16.
 */
public abstract class Action {

    public final DynamicWorld world;
    public final RobotType thisRobot;
    public final ActionType type;
    public final VectorGeometry target;


    public Action(ActionType type, DynamicWorld currentState, RobotType thisRobot, VectorGeometry target) {
        this.type = type;
        this.world = currentState;
        this.thisRobot = thisRobot;
        this.target = target;
    }

    /**
     * Checks whether it is possible to execute this action in the world passed as an argument
     *
     * @param world state of the world to be checked
     * @return true if the action is possible given the world
     */
    abstract public boolean isPossible(DynamicWorld world);

    public boolean isPossible() {
        return isPossible(this.world);
    }

    /**
     * Checks whether the current state can be still considered
     * @return
     */
    abstract public boolean deviated(DynamicWorld latestWorld);

    abstract public DynamicWorld getFutureWorld();

    abstract public double cost();


    /**
     * When we decide on a new plan, the robot is likely to be in the middle of executing an action. Some actions can
     * be easily interrupted (i.e. "hey, stop moving and kick instead!") whereas others require to be completed so that
     * the robot never gets into an undefined state. Such actions include kicking or grabbing -- we need to allow the
     * kicker and the grabber to finish their routine so that we can later kick again with proper force and actually
     * grab/ungrab.
     * @return true if the action can be interrupted
     */
    abstract public boolean isInterruptible();


    abstract public ActionHandler createActionHandler();

}
