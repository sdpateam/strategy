package strategy.action;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import strategy.executor.ActionHandler;
import strategy.executor.SynchroniseHandler;
import vision.tools.VectorGeometry;

public class Synchronise extends Action {

    public Synchronise(DynamicWorld currentState, RobotType thisRobot, VectorGeometry target) {
        super(ActionType.SYNCHRONISE, currentState, thisRobot, target);
    }

    @Override
    public boolean isPossible(DynamicWorld world) {
        return true;
    }

    @Override
    public boolean deviated(DynamicWorld latestWorld) {
        // Never deviates.
        return false;
    }

    @Override
    public DynamicWorld getFutureWorld() {
        return world;
    }

    @Override
    public double cost() {
        return 0;
    }

    @Override
    public boolean isInterruptible() {
        return true;
    }

    @Override
    public ActionHandler createActionHandler() {
        return new SynchroniseHandler();
    }
}
