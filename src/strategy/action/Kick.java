package strategy.action;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import strategy.executor.ActionHandler;
import strategy.executor.KickHandler;
import vision.tools.VectorGeometry;

/**
 * Part of project strategy
 * <p/>
 * Created by Jano Horvath on 17/02/16.
 */
public class Kick extends Action {

    /**
     * Kicks the ball if the robot has it. None of the arguments matter.
     *
     * @param currentState
     * @param thisRobot
     * @param target
     */
    public Kick(DynamicWorld currentState, RobotType thisRobot, VectorGeometry target) {
        super(ActionType.KICK, currentState, thisRobot, target);
    }

    @Override
    public boolean isPossible(DynamicWorld world) {
        return world.getProbableBallHolder() == this.thisRobot;
    }

    @Override
    public boolean deviated(DynamicWorld latestWorld) {
        // Never deviates.
        return false;
    }

    @Override
    public DynamicWorld getFutureWorld() {
        //TODO: how detailed should this be?
        return null;
    }

    @Override
    public double cost() {
        return 1;
    }

    @Override
    public boolean isInterruptible() {
        return false;
    }

    @Override
    public ActionHandler createActionHandler() {
        return new KickHandler();
    }
}
