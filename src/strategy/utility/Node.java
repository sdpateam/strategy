package strategy.utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Part of project strategy.strategy
 * <p/>
 * Basic implementation of the tree datastructure
 * <p/>
 * Created by Jano Horvath on 30/01/16.
 */
public class Node<T> {
    public T data;
    public Node<T> parent;
    public List<Node<T>> children;

    public Node(Node<T> parent, T data) {
        this.data = data;
        this.parent = parent;
        this.children = new ArrayList<>();
    }
}
