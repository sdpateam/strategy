package strategy.utility;

/**
 * Created by s1309061 on 10/03/16.
 */
public class Tuple<L, R> {

    public L left;
    public R right;

    public Tuple(L left, R right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int hashCode() {
        return left.hashCode() ^ right.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Tuple)) return false;
        Tuple Tupleo = (Tuple) o;
        return this.left.equals(Tupleo.left) &&
                this.right.equals(Tupleo.right);
    }

}
