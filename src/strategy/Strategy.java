package strategy;

import semicomms.Communication;
import strategy.commons.DynamicWorld;
import vision.RobotType;
import strategy.executor.Executor;
import strategy.goal.*;
import vision.Vision;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Part of project strategy.strategy
 * <p/>
 * Created by Jano Horvath on 27/01/16.
 */

public class Strategy {
    private static List<RobotType> activeRobots;

    private BufferedReader console;

    private Planner planner;
    private Executor executor;
    private WorldCalculator worldCalculator;
    private GoalCalculator goalCalculator;

    private Communication comms;

    private Boolean running = true;

    public Strategy(String[] args) {
        // Initializes vision
        Vision vision = new Vision(args);

        // Initializes communications
        comms = Communication.getCommunication(activeRobots);

        // Initializes a stream reader, which scans console
        console = new BufferedReader(new InputStreamReader(System.in));

//        readLine();

        // Initializes the other strategy modules
        worldCalculator = new WorldCalculator();
        vision.addVisionListener(worldCalculator);
        worldCalculator.setActiveRobots(activeRobots);

        goalCalculator = new GoalCalculator(worldCalculator);
        vision.addVisionListener(goalCalculator);

        executor = new Executor(worldCalculator); // Please leave this here....

        planner = new Planner(goalCalculator, worldCalculator, executor, activeRobots);
        vision.addVisionListener(planner); // Please leave this here...

        vision.addVisionListener(executor); // Please leave this here....
        comms.addCommunicationListener(executor);

        System.out.print("To interact with the program, type a command and hit enter anytime during execution. \n" +
                " 'help' for list of commands \n" +
                " 'exit' to exit." +
                "\n\n>> ");

        // The main loop
        while (running) {
            // Checks for user input
            checkUserCommand();
        }
        executor.abortPlans();
        comms.closePorts();
        System.out.println("\nStrategy module done. Don't forget to close the vision window to stop the vision feed.");

    }

    private void checkUserCommand() {
        try {
            if (console.ready()) {
                String commmand = console.readLine().toLowerCase();
                String answer;
                switch (commmand) {
                    case "help":
                        System.out.println(
                                "List of available commands: \n" +
                                        " s (stops everything)" +
                                        " g,k,ki,p (used for smoother play of Friend1)" +
                                        " kickoff o \n" +
                                        " kickoff d \n" +
                                        " penalty\n" +
                                        " getball \n" +
                                        " pause \n" +
                                        " unpause \n" +
                                        " reset \n" +
                                        " remove bot\n" +
                                        " add bot\n" +
                                        " set attack direction\n" +
                                        " set granularity \n" +
                                        " exit");
                        break;
                    //Become a goalie
                    case "g":
                        goalCalculator.setMainGoal(new Goalie(RobotType.FRIEND_1), RobotType.FRIEND_1);
                        break;
                    //Transmits a command directly
                    case "say":
                        System.out.print("Type in command to send: ");
                        comms.rawSend(RobotType.FRIEND_1, console.readLine());
                        break;
                    //Stops everything
                    case "s":
                        planner.paused = true;
                        executor.abortPlans();
                        break;
                    //Tries to perform a kickoff
                    case "k":
                        goalCalculator.setMainGoal(new Kickoff(RobotType.FRIEND_1), RobotType.FRIEND_1);
                        break;
                    //Penalty macro
                    case "p":
                        System.out.println("Kicking a penalty");
                        goalCalculator.setMainGoal(new Penalty(RobotType.FRIEND_1), RobotType.FRIEND_1);
                        break;

                    case "kickoff d" :
                        System.out.print("  Which robot should become the goalie? enter f1 or f2 and PRESS ENTER " +
                                "after they kick the ball.: ");
                        answer = console.readLine();
                        // Right now it is very conservative. Sends one robot to be the goalie, the other one to
                        // retrieve the ball
                        if (answer.equals("f1")) {
                            goalCalculator.setMainGoal(new Goalie(RobotType.FRIEND_1), RobotType.FRIEND_1);
                            goalCalculator.setMainGoal(new GetBall(RobotType.FRIEND_2), RobotType.FRIEND_2);
                        } else if (answer.equals("f2")) {
                            goalCalculator.setMainGoal(new Goalie(RobotType.FRIEND_2), RobotType.FRIEND_2);
                            goalCalculator.setMainGoal(new GetBall(RobotType.FRIEND_1), RobotType.FRIEND_1);
                        } else {
                            System.out.println("  Invalid answer. No action scheduled.");
                        }
                        break;

                    case "reset":
                        System.out.println("  Sending robots to default positions.");
                        DynamicWorld world = worldCalculator.getCalculatedWorld();
                        if (world.attackingPositive) {
                            goalCalculator.setMainGoal(new GoTo(RobotType.FRIEND_1, world.FRIEND_DEF_TOP_RIGHT), RobotType.FRIEND_1);
                            goalCalculator.setMainGoal(new GoTo(RobotType.FRIEND_2, world.FRIEND_DEF_BOTTOM_RIGHT), RobotType.FRIEND_2);
                        } else {
                            goalCalculator.setMainGoal(new GoTo(RobotType.FRIEND_1, world.FRIEND_DEF_TOP_LEFT), RobotType.FRIEND_1);
                            goalCalculator.setMainGoal(new GoTo(RobotType.FRIEND_2, world.FRIEND_DEF_BOTTOM_LEFT), RobotType.FRIEND_2);
                        }
                        break;

                    case "pause":
                        System.out.println("  Pausing the strategy module.");
                        planner.paused = true;
                        executor.abortPlans();

                    case "unpause":
                        System.out.println("  Unpaused.");
                        planner.paused = false;
                        executor.abortPlans();

                    case "penalty":
                        System.out.print("  Which robot should kick penalty? (f1/f2): ");
                        answer = console.readLine();
                        if (answer.equals("f1")) {
                            goalCalculator.setMainGoal(new Penalty(RobotType.FRIEND_1), RobotType.FRIEND_1);
                            goalCalculator.setMainGoal(new Goalie(RobotType.FRIEND_2), RobotType.FRIEND_2);
                        } else if (answer.equals("f2")) {
                            goalCalculator.setMainGoal(new Penalty(RobotType.FRIEND_2), RobotType.FRIEND_2);
                            goalCalculator.setMainGoal(new Goalie(RobotType.FRIEND_1), RobotType.FRIEND_1);
                        } else {
                            System.out.println("  Invalid answer. No action scheduled.");
                        }
                        break;

                    case "remove bot":
                        System.out.print("  Currently in control of ");
                        for(RobotType robotType : activeRobots) {
                            System.out.print(robotType.name() + " ");
                        }
                        System.out.print("\n  Which bot should be removed? (f1/f2): ");
                        answer = console.readLine();
                        if (answer.equals("f1")) {
                            activeRobots.remove(RobotType.FRIEND_1);
                        } else if (answer.equals("f2")) {
                            activeRobots.remove(RobotType.FRIEND_2);
                        } else {
                            System.out.println("  Invalid answer. Robot not removed.");
                        }
                        worldCalculator.setActiveRobots(activeRobots);
                        planner.forceReplan();
                        break;

                    case "add bot":
                        System.out.print("  Currently in control of ");
                        for(RobotType robotType : activeRobots) {
                            System.out.print(robotType.name() + " ");
                        }
                        System.out.print("\n  Which bot should be added? (f1/f2): ");
                        answer = console.readLine();
                        if (answer.equals("f1")) {
                            activeRobots.add(RobotType.FRIEND_1);
                            comms = Communication.getCommunication(activeRobots);
                        } else if (answer.equals("f2")) {
                            activeRobots.add(RobotType.FRIEND_2);
                            comms = Communication.getCommunication(activeRobots);
                        } else {
                            System.out.println("  Invalid answer. Robot not removed.");
                        }
                        worldCalculator.setActiveRobots(activeRobots);
                        planner.forceReplan();
                        break;

                    case "set ad":
                        System.out.println("  Attacking positive: " + worldCalculator.isAttackingPositive());
                        System.out.print("  Shall we attack positive? (Y/N): ");
                        answer = console.readLine();
                        if (answer.equals("Y")) {
                            worldCalculator.setAttackingPositive(true);
                        } else if (answer.equals("N")) {
                            worldCalculator.setAttackingPositive(false);
                        } else {
                            System.out.println("  Invalid answer. Value not set.");
                        }
                        break;

                    case "set granularity":
                        System.out.println("  Current granularity is: " + worldCalculator.getGranularity());
                        System.out.print("  Enter new value: ");
                        worldCalculator.setGranularity(Integer.parseInt(console.readLine()));
                        break;

                    case "exit":
                        running = false;
                        break;
                    default:
                        System.out.println("I don't understand '" + commmand + "'. Continuing simulation.");
                }

                if (!commmand.equals("exit")) System.out.print("\n>> ");
            }
        } catch (IOException e) {
            System.out.println("Console crashed ?!");
            e.printStackTrace();
        }
    }

    // Utility class
    private String readLine() {
        try {
            String s = console.readLine();
            return s;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Main method. Serves as a argument parser.
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("This is the control of the SDP robots, developed by the A Team in year 2016AD.\n");

        if (args.length == 0 || args[0].toLowerCase().contains("help")) {
            System.out.println(
                    "**** How to use this ****\n" +
                            "[Required] The first argument specifies whether we are controlling robot of group1, group2 or " +
                            "both.\n" +
                            "  use -g1 -g2 -g12 respectively. For developing without RFStick use -dev. \n\n" +
                            "[Optional] The rest of the command line args are used to automate initializing the vision module" +
                            " and their detailed description can be found in the vision docs.\n\n");
        } else {
            // Parses args to figure out which robots to control
            activeRobots = new ArrayList<>();
            switch (args[0]) {
                case ("-g1"): {
                    activeRobots.add(RobotType.FRIEND_1);
                    break;
                }
                case ("-g2"): {
                    activeRobots.add(RobotType.FRIEND_2);
                    break;
                }
                case ("-g12"): {
                    activeRobots.add(RobotType.FRIEND_1);
                    activeRobots.add(RobotType.FRIEND_2);
                    break;
                }
            }
            new Strategy(args);
        }
    }
}
