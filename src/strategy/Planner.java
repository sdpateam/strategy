package strategy;

import strategy.action.*;
import strategy.commons.DynamicWorld;
import strategy.commons.Geometry;
import vision.RobotType;
import strategy.executor.Executor;
import strategy.goal.Goal;
import strategy.goal.GoalCalculator;
import strategy.utility.Tuple;
import vision.VisionListener;
import vision.tools.Point;
import vision.tools.VectorGeometry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Part of project strategy
 * <p/>
 * Created by Jano Horvath on 04/03/16.
 */

public class Planner implements VisionListener {

    private WorldCalculator world_calculator;
    private GoalCalculator  goal_calculator;
    private Executor executor;
    private List<RobotType> active_robots;

    public boolean paused = true;

    public Planner(GoalCalculator goal_calculator, WorldCalculator world_calculator, Executor executor, List<RobotType> active_robots) {
        this.world_calculator = world_calculator;  // To get current world

        this.goal_calculator  = goal_calculator;   // To get current goals
        this.goal_calculator.linkPlanner(this);

        this.active_robots    = active_robots;     // List of current active and friendly robots.

        this.executor         = executor;          // So that planner can push plans to executor
    }

    public Plan getPlan(List<RobotType> active_robots) {
        // Calculates a plan consisting of the world, two lists of actions (one for each robot), and it's estimated cost.
        DynamicWorld world = world_calculator.getCalculatedWorld();  // Get most current world.

        Tuple<List<Action>, Double> friend1Plan = new Tuple<List<Action>, Double>(new ArrayList<Action>(), 0.0);  // Initialise Individual plans, type is a tuple of the list of actions and the cost of the plan.
        Tuple<List<Action>, Double> friend2Plan = new Tuple<List<Action>, Double>(new ArrayList<Action>(), 0.0);

        for (RobotType robot: active_robots) {  // For each friendly robot currently in play;
            if (robot == RobotType.FRIEND_1) {
                friend1Plan = getIndividualPlan(world, robot, goal_calculator.getCurrentGoal(robot));  // If FRIEND_1, retrieve goal and calculate individual plan for FRIEND_1.

            } else {
                friend2Plan = getIndividualPlan(world, robot, goal_calculator.getCurrentGoal(robot));  // Same for FRIEND_2.
            }
        }

        return new Plan(world, friend1Plan.left, friend2Plan.left, Math.max(friend1Plan.right, friend2Plan.right));  // Return plan, with the cost being the max of the costs of the two individual plans.
    }

    private Tuple<List<Action>, Double> getIndividualPlan(DynamicWorld world, RobotType robot, Goal sub_goal) {
        Tuple<List<Action>, Double> output = new Tuple<List<Action>, Double>(new ArrayList<Action>(), 0.0);
        Action          action;
        VectorGeometry  target;

        switch (sub_goal.type) {
            case SHOOT:
                // Turn to face goal.
                action = new RotateTowards(world, robot, Geometry.midpoint(world.FOE_GOAL_POST_TOP, world.FOE_GOAL_POST_BOTTOM));
                output.left.add(action);
                output.right = cost(action);

                // Kick.
                action = new Kick(world, robot, null);
                output.left.add(action);
                output.right += cost(action);

                // End.
                break;

            case KICKOFF:
                // Turn to face goal.
                action = new RotateTowards(world, robot, Geometry.midpoint(world.FOE_GOAL_POST_TOP, world.FOE_GOAL_POST_BOTTOM));
                output.left.add(action);
                output.right = cost(action);

                // Kick.
                action = new Kick(world, robot, null);
                output.left.add(action);
                output.right += cost(action);

                // End.
                break;

            case STOP:
                // Stop! Just an empty plan.
                break;

            case MAKE_PASS:
                // Rotate to be facing pass destination (target.)
                target = sub_goal.getTargetPoint(world);
                action = new RotateTowards(world, robot, target);
                output.left.add(action);
                output.right = cost(action);

                // Sync.
                action = new Synchronise(world, robot, null);
                output.left.add(action);
                output.right += cost(action);

                // Kick.
                action = new Kick(world, robot, null);
                output.left.add(action);
                output.right += cost(action);

                // End.
                break;

            case RECEIVE_PASS:
                // Fetches location of passer.
                if (robot == RobotType.FRIEND_1) {
                    target = world.getRobot(RobotType.FRIEND_2).location;

                } else {
                    target = world.getRobot(RobotType.FRIEND_1).location;
                }

                // Goto given pass destination (target.) Includes rotation so robot will face the passer.
                generateMovements(world, robot, sub_goal.getTargetPoint(world), target, output);

                // Sync.
                action = new Synchronise(world, robot, null);
                output.left.add(action);
                output.right += cost(action);

                // End.
                break;

            case GOTO:
                // Goto given pos.
                generateMovements(world, robot, sub_goal.getTargetPoint(world), null, output);

                // End.
                break;

            case GOALIE:
                // Goto own goal (aka n cm in front of own goal).
                int n = 20;
                VectorGeometry topPoint, bottomPoint;
                if (world.attackingPositive) {
                    topPoint = new VectorGeometry(world.FRIEND_GOAL_POST_TOP.x + n, world.FRIEND_GOAL_POST_BOTTOM.y);
                    bottomPoint = new VectorGeometry(world.FRIEND_GOAL_POST_BOTTOM.x + n, world.FRIEND_GOAL_POST_BOTTOM.y);
                } else {
                    topPoint = new VectorGeometry(world.FRIEND_GOAL_POST_BOTTOM.x - n, world.FRIEND_GOAL_POST_BOTTOM.y);
                    bottomPoint = new VectorGeometry(world.FRIEND_GOAL_POST_TOP.x - n, world.FRIEND_GOAL_POST_TOP.y);
                }
                target = Geometry.midpoint( topPoint, bottomPoint);
                generateMovements(world, robot, target, null, output);

                // Rotate towards the center of the pitch
                // NB: this uses the wrong world, but the executor should handle that
                output.left.add(new RotateTowards(world, robot, new VectorGeometry(0,0)));

                // Strafe n times up-down
                Action moveUp = new HolonomicMove(world, robot, topPoint);
                Action moveDown = new HolonomicMove(world, robot, bottomPoint);
                for (int i = 0; i < 20; i++) {
                    output.left.add(moveUp);
                    output.left.add(moveDown);
                }

                // End.
                break;

            case BLOCK_PASS:
                // Goto pos inbetween enemies.
                if (world.getRobot(RobotType.FOE_1) == null || world.getRobot(RobotType.FOE_2) == null) {
                    System.out.println("[Planner] can't schedule pass intercept if there aren't two foes on the pitch");
                    break;
                }
                target = Geometry.midpoint(world.getRobot(RobotType.FOE_1).location, world.getRobot(RobotType.FOE_2).location);
                generateMovements(world, robot, target, null, output);

                // End.
                break;

            case GET_BALL:
                // Goto ball.
//                while (robot != world.robotWithBall) {
                    generateMovements(world, robot, world.getBall().location, world.getBall().location, output);
//                }

                // Grab.
                action = new Grab(world, robot, null);
                output.left.add(action);
                output.right += cost(action);

                // End.
                break;

            case PENALTY:
                // Rotate between enemy goal posts for confusion.
                for (int i = 0; i < 2; i++) {
                    action = new RotateTowards(world, robot, world.FOE_GOAL_POST_BOTTOM);
                    output.left.add(action);
                    action = new RotateTowards(world, robot, world.FOE_GOAL_POST_TOP);
                    output.left.add(action);
                }

                // Calculating where to shoot.
                if (world.attackingPositive) {
                    target = new VectorGeometry(world.FOE_GOAL_POST_TOP.x, -10/world.granularity);
                } else {
                    target = new VectorGeometry(world.FOE_GOAL_POST_TOP.x, 10/world.granularity);
                }
                //Double goal_width = Geometry.distance(world.FOE_GOAL_POST_BOTTOM, world.FOE_GOAL_POST_TOP);
                //target.x += (goal_width / 2) - 5/world.granularity;

                // Face target.
                action = new RotateTowards(world, robot, target);
                output.left.add(action);

                // Fire!
                action = new Kick(world, robot, null);
                output.left.add(action);

                // End.
                break;
        }
        return output;
    }

    private void generateMovements(DynamicWorld world, RobotType robot, VectorGeometry target, VectorGeometry rotation_target, Tuple<List<Action>, Double> current_plan) {
        // Moves given robot to target point with provided change in direction, and adds it to the current plan. Use null for rotation target if you don't care about resulting direction. Note: doesn't yet avoid obstacles, nor calculate weather it would be faster to move backwards or forwards.
        List<Action> movements = new ArrayList<>();
        Action move;

//        if (robot == RobotType.FRIEND_1) {  // Currently unused, as both robots have the same movement commands at present.
//            if (rotation_target != null) {  // If we care about the final direction;
//                Point desiredVector = new Point(rotation_target.x - target.x, rotation_target.y - target.y);
//
//                // TODO: what if current.direction is > 2pi
//                Double angle                  = Geometry.angle(desiredVector.x, desiredVector.y);
//                DirectedPoint directed_target = new DirectedPoint(target.x, target.y, angle);
//                move                          = new HolonomicMoveAndRotate(world, robot, directed_target);
//                movements.add(move);
//
//            } else {
//                move = new HolonomicMove(world, robot, target);
//                movements.add(move);
//            }
//
//        } else {
            move = new MoveForwardsOrBackwards(world, robot, target);

            if (false) { // If we can move without rotating, do that.
                movements.add(move);

                if (rotation_target != null) {
                    movements.add(new RotateTowards(world, robot, rotation_target));  // Add rotation if needed.
                }

            } else {
//                Double angle1 = Geometry.angle(world.getRobot(robot), target);  // Angle to move forwards.
//                Double angle2 = Math.PI - angle1;                               // Angle to move backwards.

//                if (rotation_target != null) {
//                    DirectedPoint movement_vector1 = new DirectedPoint(world.getRobot(robot).x, world.getRobot(robot).y, world.getRobot(robot).direction + angle1);
//                    DirectedPoint movement_vector2 = new DirectedPoint(world.getRobot(robot).x, world.getRobot(robot).y, world.getRobot(robot).direction + angle2);
//
//                    angle1 += Geometry.angle(movement_vector1, rotation_target);
//                    angle2 += Geometry.angle(movement_vector2, rotation_target);
//                }

//                if (angle1 <= angle2) { // Works out if it is better to rotate towards target, move forwards and rotate or rotate away from target, move backwards and rotate.
                movements.add(new RotateTowards(world, robot, target));
                movements.add(new MoveForwardsOrBackwards(world, robot, target));

                if (rotation_target != null) {
                    movements.add(new RotateTowards(world, robot, rotation_target));
                }
//
//                } else {
//                    Point inverted_target = new Point(-target.x, -target.y);
//                    movements.add(new RotateTowards(world, robot, inverted_target));
//                    movements.add(new MoveForwardsOrBackwards(world, robot, target));
//
//                    if (rotation_target != null) {
//                        movements.add(new RotateTowards(world, robot, rotation_target));
//                    }
//                }
//            }
        }

        for (Action a : movements) {
            current_plan.left.add(a);
            current_plan.right = current_plan.right + cost(a);
        }
    }

    private double cost(Action action) {
        // Returns the cost of the given action.
        double friend_1_grab_scalar        = 1;  // These will be updated with real measurements.
        double friend_1_kick_scalar        = 1;
        double friend_1_move_scalar        = 1;
        double friend_1_rotate_scalar      = 1;
        double friend_1_synchronise_scalar = 1;

        double friend_2_grab_scalar        = 1;
        double friend_2_kick_scalar        = 1;
        double friend_2_move_scalar        = 1;  // Does friend2 need different scalars for moving forwards and moving backwards?
        double friend_2_rotate_scalar      = 1;
        double friend_2_synchronise_scalar = 1;

        double result_scalar = 0;

        switch (action.type) {
            case GRAB:
                switch(action.thisRobot) {
                    case FRIEND_1:
                        result_scalar = friend_1_grab_scalar;
                        break;

                    case FRIEND_2:
                        result_scalar = friend_2_grab_scalar;
                        break;
                }
                break;

            case KICK:
                switch(action.thisRobot) {
                    case FRIEND_1:
                        result_scalar = friend_1_kick_scalar;
                        break;

                    case FRIEND_2:
                        result_scalar = friend_2_kick_scalar;
                        break;
                }
                break;

            case MOVE_FORWARDS_OR_BACKWARDS:
                switch(action.thisRobot) {
                    case FRIEND_1:
                        result_scalar = friend_1_move_scalar;
                        break;

                    case FRIEND_2:
                        result_scalar = friend_2_move_scalar;
                        break;
                }
                break;

            case ROTATE_TOWARDS:
                switch(action.thisRobot) {
                    case FRIEND_1:
                        result_scalar = friend_1_rotate_scalar;
                        break;

                    case FRIEND_2:
                        result_scalar = friend_2_rotate_scalar;
                        break;
                }
                break;

            case SYNCHRONISE:  // I don't think there will be different scalars for synchronise, but I'm including it anyway.
                switch(action.thisRobot) {
                    case FRIEND_1:
                        result_scalar = friend_1_synchronise_scalar;
                        break;

                    case FRIEND_2:
                        result_scalar = friend_2_synchronise_scalar;
                        break;
                }
                break;
        }

        return result_scalar * action.cost();
    }

    public void forceReplan() {
        DynamicWorld world = world_calculator.getCalculatedWorld();
        active_robots = world.activeRobots;
        Plan newestPlan = getPlan(active_robots);

        HashMap<RobotType, List<Action>> hashedPlan = new HashMap<>();
        hashedPlan.put(RobotType.FRIEND_1, newestPlan.friend1_plan);
        hashedPlan.put(RobotType.FRIEND_2, newestPlan.friend2_plan);

        executor.setNewPlans(hashedPlan);

        paused = false;
    }

    @Override
    public void nextWorld(vision.DynamicWorld dynamicWorld) {
        if (paused) return;

        DynamicWorld world = world_calculator.getCalculatedWorld();
        active_robots = world.activeRobots;
        Plan newestPlan = getPlan(active_robots);

        HashMap<RobotType, List<Action>> hashedPlan = new HashMap<>();
        hashedPlan.put(RobotType.FRIEND_1, newestPlan.friend1_plan);
        hashedPlan.put(RobotType.FRIEND_2, newestPlan.friend2_plan);

        // Checks whether the new plan should be pushed to the executor
        for( RobotType robotType : active_robots) {
            List<Action> executedPlan = executor.plans.get(robotType);

            // #1 - Robot is idle
            if (executedPlan.size() == 0) {
                executor.setNewPlans(hashedPlan);
                return;
            }

            // #2 - The current action is not possible
            Action currentAction = executedPlan.get(0);
            if (!currentAction.isPossible()) {
                executor.setNewPlans(hashedPlan);
                return;
            }

            // #3 - The robot has deviated from the current action (shame on the executor)
            if (currentAction.deviated(world)) {
                executor.setNewPlans(hashedPlan);
                return;
            }
        }
    }
}
