package strategy;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import vision.VisionListener;

import java.util.ArrayList;
import java.util.List;

public class WorldCalculator implements VisionListener {

    private DynamicWorld currentWorld;

    private int granularity = 5;

    private boolean attackingPositive = true;

    private List<RobotType> activeRobots = new ArrayList<>();

    public DynamicWorld getCalculatedWorld() {
        if (currentWorld == null) {
            System.out.println("Careful! The vision has not sent a frame yet. You are about to get a NullPointer" +
                    " greeting..");
        }
        return currentWorld;
    }

    public void grabbedBall(RobotType robot) {
        // If vision reports the ball is in front of the robot
        if (currentWorld.getProbableBallHolder() == robot ) {
            currentWorld.robotWithBall = robot;
        }
    }

    public void releasedBall(RobotType robot) {
        if (currentWorld.robotWithBall == robot) {
            currentWorld.robotWithBall = null;
        }
    }

    /**
     * Sets the granularity of squares the next world received from vision will be sliced into
     *
     * @param granularity dimension of square in cm
     */
    public void setGranularity(int granularity) {
        this.granularity = granularity;
    }

    public int getGranularity() {
        return granularity;
    }

    /**
     * Provides information about which side of pitch we are attacking
     * @return
     */
    public boolean isAttackingPositive() {
        return attackingPositive;
    }

    public void setAttackingPositive(boolean attackingPositive) {
        this.attackingPositive = attackingPositive;
    }

    public void setActiveRobots(List<RobotType> activeRobots) {
        this.activeRobots = activeRobots;
    }

    @Override
    public void nextWorld(vision.DynamicWorld dynamicWorld) {
        // By passing currentWorld as an argument to the constructor, we achieve a primitive type of smoothing, where if
        // an object was once reported to be seen by the vision, its position will propagate through new calculatedWorld
        // objects unless updated by a new position information from the vision.
        currentWorld = new DynamicWorld(dynamicWorld, currentWorld, granularity, attackingPositive, activeRobots);
    }
}
