package strategy.goal;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import vision.tools.VectorGeometry;

/**
 * Part of project strategy
 *
 * Created by Jano Horvath on 11/03/16.
 */
public class Goalie extends Goal {
    public Goalie(RobotType robot) {
        super(GoalType.GOALIE, robot);

        //has no prereqs.
    }

    @Override
    public VectorGeometry getTargetPoint(DynamicWorld world) {
        if (world.attackingPositive) {
            return new VectorGeometry(-world.PITCH_HALF_LENGTH, 0);
        } else {
            return new VectorGeometry(world.PITCH_HALF_LENGTH, 0);
        }
    }

    @Override
    public boolean wasExecuted(DynamicWorld calculatedWorld) {
        // This should never get executed, only cancelled by a turnover
        return false;
    }
}
