package strategy.goal;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import vision.tools.VectorGeometry;

import java.util.ArrayList;
import java.util.List;

/**
 * Part of project strategy
 * <p/>
 * Created by Jano Horvath on 27/02/16.
 */
public abstract class Goal {
    public RobotType robot;
    public GoalType type;

    // LIFO! Last prereq shall be scheduled first (e.g. for pass you have to [FACE_MATE, GET_BALL] rather than other
    // way around)
    public List<GoalType> prereqs;

    public Goal(GoalType type, RobotType robot) {
        this.type = type;
        this.robot = robot;
        this.prereqs = new ArrayList<>();
    }

    public abstract VectorGeometry getTargetPoint(DynamicWorld world);

    public abstract boolean wasExecuted(DynamicWorld calculatedWorld);
}
