package strategy.goal;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import vision.tools.VectorGeometry;

/**
 * Created by s1309061 on 14/03/16.
 */
public class Stop extends Goal{
    public Stop(RobotType robot) {
        super(GoalType.STOP, robot);
    }

    @Override
    public VectorGeometry getTargetPoint(DynamicWorld world) {
        return null;
    }

    @Override
    public boolean wasExecuted(DynamicWorld calculatedWorld) {
        // Should always be executed.
        return true;
    }
}

