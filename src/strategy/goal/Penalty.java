package strategy.goal;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import vision.tools.VectorGeometry;

/**
 * Created by Mark on 12/03/2016.
 */
public class Penalty extends Goal {
    public Penalty(RobotType robot) {
        super(GoalType.PENALTY, robot);

        // Do we need a get ball prereq?
        // Jano: No we don't, according to the rules the robot has the ball
    }

    @Override
    public VectorGeometry getTargetPoint(DynamicWorld world) {
        return null;
    }

    @Override
    public boolean wasExecuted(DynamicWorld calculatedWorld) {
        return false; //shady fix. But we are manually controlling penalties anyway..
    }
}
