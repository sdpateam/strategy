package strategy.goal;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import vision.tools.VectorGeometry;

/**
 * Part of project strategy
 *
 * Created by Jano Horvath on 14/03/16.
 */
public class Kickoff extends Goal {
    public Kickoff(RobotType robot) {
        super(GoalType.KICKOFF, robot);

        prereqs.add(GoalType.GET_BALL);
    }

    @Override
    public VectorGeometry getTargetPoint(DynamicWorld world) {
        // Just boots the ball for now
        return world.FOE_DEF_BOTTOM_LEFT;
    }

    @Override
    public boolean wasExecuted(DynamicWorld calculatedWorld) {
        // Is executed when the ball crosses one tenth of the opponents half of the pitch
        if (calculatedWorld.attackingPositive) {
            return calculatedWorld.getBall().location.x > calculatedWorld.PITCH_HALF_LENGTH/10;
        } else {
            return calculatedWorld.getBall().location.x < -calculatedWorld.PITCH_HALF_LENGTH/10;
        }
    }
}
