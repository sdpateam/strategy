package strategy.goal;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import vision.tools.DirectedPoint;
import vision.tools.VectorGeometry;

/**
 * Part of project strategy
 * <p/>
 * Created by Jano Horvath on 27/02/16.
 */
public class MakePass extends Goal {

    public MakePass(RobotType robot) {
        super(GoalType.MAKE_PASS, robot);

        this.prereqs.add(GoalType.GET_BALL);
    }

    @Override
    public VectorGeometry getTargetPoint(DynamicWorld world) {
        DirectedPoint matePos;

        if (this.robot == RobotType.FRIEND_1) {
            matePos = world.getRobot(RobotType.FRIEND_2).location;
        } else {
            matePos = world.getRobot(RobotType.FRIEND_1).location;
        }

        return new VectorGeometry(matePos.x, matePos.y);
    }

    @Override
    public boolean wasExecuted(DynamicWorld calculatedWorld) {
        return calculatedWorld.getProbableBallHolder() != this.robot;
    }
}
