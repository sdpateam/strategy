package strategy.goal;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import vision.tools.VectorGeometry;

import static strategy.commons.Constants.CENTER_OF_GOAL;

/**
 * Part of project strategy
 * <p/>
 * Created by Jano Horvath on 27/02/16.
 */
public class Shoot extends Goal {
    public Shoot(RobotType robot) {
        super(GoalType.SHOOT, robot);

        this.prereqs.add(GoalType.GET_BALL);
    }

    @Override
    public VectorGeometry getTargetPoint(DynamicWorld world) {
        // The center of the goal place.
        return CENTER_OF_GOAL;
    }

    @Override
    public boolean wasExecuted(DynamicWorld calculatedWorld) {
        return calculatedWorld.getProbableBallHolder() != this.robot;
    }
}
