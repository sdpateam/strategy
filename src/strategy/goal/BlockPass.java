package strategy.goal;

import strategy.commons.DynamicWorld;
import strategy.commons.Geometry;
import vision.RobotType;
import vision.tools.VectorGeometry;

/**
 * Part of project strategy
 *
 * Created by Jano Horvath on 11/03/16.
 */
public class BlockPass extends Goal {
    public BlockPass(RobotType robot) {
        super(GoalType.BLOCK_PASS, robot);
    }

    @Override
    public VectorGeometry getTargetPoint(DynamicWorld world) {
        VectorGeometry foe1 = world.getRobot(RobotType.FOE_1).location;
        VectorGeometry foe2 = world.getRobot(RobotType.FOE_2).location;

        // If both robots are on the pitch
        if (foe1 != null && foe2 != null) {
            return Geometry.midpoint(foe1, foe2);

        // else blocks random angle
        } else {
            return world.FRIEND_DEF_TOP_LEFT;
        }
    }

    @Override
    public boolean wasExecuted(DynamicWorld calculatedWorld) {
        // This should never get executed, only cancelled by a turnover
        return false;
    }
}
