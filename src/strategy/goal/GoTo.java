package strategy.goal;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import vision.tools.VectorGeometry;

/**
 * Part of project strategy
 *
 * Created by Jano Horvath on 14/03/16.
 */
public class GoTo extends Goal {

    private VectorGeometry target;

    public GoTo(RobotType robot, VectorGeometry target) {
        super(GoalType.GOTO, robot);

        this.target = target;
    }

    @Override
    public VectorGeometry getTargetPoint(DynamicWorld world) {
        return target;
    }

    @Override
    public boolean wasExecuted(DynamicWorld calculatedWorld) {
        // If it came at least 5cm close to the target
        return Math.abs(calculatedWorld.getRobot(robot).location.x - target.x) < 5/calculatedWorld.granularity
            && Math.abs(calculatedWorld.getRobot(robot).location.y - target.y) < 5/calculatedWorld.granularity;
    }
}
