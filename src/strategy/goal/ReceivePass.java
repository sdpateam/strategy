package strategy.goal;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import vision.tools.VectorGeometry;

/**
 * Part of project strategy
 *
 * Created by Jano Horvath on 11/03/16.
 */
public class ReceivePass extends Goal {
    public ReceivePass(RobotType robot) {
        super(GoalType.RECEIVE_PASS, robot);
    }

    @Override
    public VectorGeometry getTargetPoint(DynamicWorld world) {
        VectorGeometry ball = world.getBall().location;

        // If the ball is in the upper half of the pitch
        if (ball.y > 0) {
            // Returns the bottom corner of the FOE def area
            if (world.attackingPositive) {
                return world.FOE_DEF_BOTTOM_LEFT;
            } else {
                return world.FOE_DEF_BOTTOM_RIGHT;
            }
        // Else returns the top corner of the FOE def area
        } else {
            if (world.attackingPositive) {
                return world.FOE_DEF_TOP_LEFT;
            } else {
                return world.FOE_DEF_TOP_RIGHT;
            }
        }
    }

    @Override
    public boolean wasExecuted(DynamicWorld calculatedWorld) {
        return calculatedWorld.robotWithBall == this.robot;
    }
}
