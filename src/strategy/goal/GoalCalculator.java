package strategy.goal;

import strategy.Planner;
import strategy.WorldCalculator;
import strategy.commons.DynamicWorld;
import strategy.commons.Geometry;
import vision.RobotType;
import vision.VisionListener;
import vision.tools.DirectedPoint;
import vision.tools.VectorGeometry;

import java.util.ArrayList;
import java.util.List;

/**
 * Part of project strategy
 * <p/>
 * Created by Jano Horvath on 23/02/16.
 */
public class GoalCalculator implements VisionListener {

    private WorldCalculator worldCalculator;
    private Planner planner; // So that forceReplan() can be called

    private boolean attacking;

    private int f1_currentGoalIndex;
    private List<Goal> friend1_GoalQueue;

    private int f2_currentGoalIndex;
    private List<Goal> friend2_GoalQueue;

    public GoalCalculator(WorldCalculator worldCalculator) {
        this.worldCalculator = worldCalculator;
        this.friend1_GoalQueue = new ArrayList<>();
        this.friend2_GoalQueue = new ArrayList<>();
    }

    public void setMainGoal(Goal mainGoal, RobotType robotType) {
        // Determines whether we are attacking or defending
        if (mainGoal.type == GoalType.SHOOT
                || mainGoal.type == GoalType.MAKE_PASS
                || mainGoal.type == GoalType.RECEIVE_PASS) {
            attacking = true;
        }
        if (mainGoal.type == GoalType.GOALIE
                || mainGoal.type == GoalType.BLOCK_PASS) {
            attacking = false;
        }


        // Generates subgoals and sets the current goal
        if (robotType == RobotType.FRIEND_1) {
            friend1_GoalQueue.clear();
            friend1_GoalQueue.add(mainGoal);
            generateSubgoals(mainGoal, friend1_GoalQueue);
            f1_currentGoalIndex = 0;
        } else {
            friend2_GoalQueue.clear();
            friend2_GoalQueue.add(mainGoal);
            generateSubgoals(mainGoal, friend2_GoalQueue);
            f2_currentGoalIndex = 0;
        }

        // After the new goals have been set, forces a replan to happen.
        if (planner!= null) {
            planner.forceReplan();
        }
    }

    public Goal getCurrentGoal(RobotType robotType) {
        if (robotType == RobotType.FRIEND_1) {
            return friend1_GoalQueue.get(f1_currentGoalIndex);
        } else {
            return friend2_GoalQueue.get(f2_currentGoalIndex);
        }
    }

    public void generateSubgoals(Goal goal, List<Goal> goalQueue) {
        RobotType robot = goal.robot;

        for (GoalType subgoal : goal.prereqs) {
            Goal newGoal;
            switch (subgoal) {
                case SHOOT: {
                    newGoal = new Shoot(robot);
                    break;
                }
                case MAKE_PASS: {
                    newGoal = new MakePass(robot);
                    break;
                }
                case GET_BALL: {
                    newGoal = new GetBall(robot);
                    break;
                }
                default:
                    newGoal = new Shoot(robot);
            }
            goalQueue.add(0, newGoal);
            generateSubgoals(newGoal, goalQueue);
        }
    }

    public void attack() {
        // TODO: this can be much more complex
        this.attacking = true;

        DynamicWorld world = worldCalculator.getCalculatedWorld();

        VectorGeometry ball = world.getBall().location;
        DirectedPoint friend1 = world.getRobot(RobotType.FRIEND_1).location;
        DirectedPoint friend2 = world.getRobot(RobotType.FRIEND_2).location;

        // If the other friend is not there, we have to SHOOT
        if (friend1 == null) {
            setMainGoal(new Shoot(RobotType.FRIEND_2), RobotType.FRIEND_2);
            return;
        }
        if (friend2 == null) {
            setMainGoal(new Shoot(RobotType.FRIEND_1), RobotType.FRIEND_1);
            return;
        }

        // Calculates the distance from the ball
        Double distance1 = Geometry.distance(ball, friend1);
        Double distance2 = Geometry.distance(ball, friend2);

        // If the ball is close enough to shoot
        if ((world.attackingPositive && ball.x > 0)
                || (!world.attackingPositive && ball.x < 0)) {

            // Schedules the robot which is closer to the ball to SHOOT
            // TODO: what should the other robot do?
            if (distance1 <= distance2) {
                setMainGoal(new Shoot(RobotType.FRIEND_1), RobotType.FRIEND_1);
            } else {
                setMainGoal(new Shoot(RobotType.FRIEND_2), RobotType.FRIEND_2);
            }

            // If the ball is far from FOE goal post
        } else {
            // Schedules the robot which is closer to the ball to MAKE PASS
            if (distance1 <= distance2) {
                setMainGoal(new MakePass(RobotType.FRIEND_1), RobotType.FRIEND_1);
                setMainGoal(new ReceivePass(RobotType.FRIEND_2), RobotType.FRIEND_2);
            } else {
                setMainGoal(new MakePass(RobotType.FRIEND_2), RobotType.FRIEND_2);
                setMainGoal(new ReceivePass(RobotType.FRIEND_1), RobotType.FRIEND_1);
            }
        }
    }

    public void defend() {
        this.attacking = false;

        DynamicWorld world = worldCalculator.getCalculatedWorld();

        DirectedPoint friend1 = world.getRobot(RobotType.FRIEND_1).location;
        DirectedPoint friend2 = world.getRobot(RobotType.FRIEND_2).location;

        //if the other friend is not there, we have to DEFEND GOAL
        if (friend1 == null) {
            setMainGoal(new Goalie(RobotType.FRIEND_2), RobotType.FRIEND_2);
            return;
        }
        if (friend2 == null) {
            setMainGoal(new Goalie(RobotType.FRIEND_1), RobotType.FRIEND_1);
            return;
        }

        // Calculates the distance from the goal
        Double distance1 = Geometry.distance(world.FOE_GOAL_POST_TOP, friend1);
        Double distance2 = Geometry.distance(world.FOE_GOAL_POST_TOP, friend2);

        if (distance1 <= distance2) {
            setMainGoal(new Goalie(RobotType.FRIEND_1), RobotType.FRIEND_1);
            setMainGoal(new BlockPass(RobotType.FRIEND_2), RobotType.FRIEND_2);
        } else {
            setMainGoal(new Goalie(RobotType.FRIEND_2), RobotType.FRIEND_2);
            setMainGoal(new BlockPass(RobotType.FRIEND_1), RobotType.FRIEND_1);
        }
    }

    public void linkPlanner(Planner planner) {
        this.planner = planner;
    }

    /**
     * Once vision has a new frame it calls this method
     * This will check whether the current subgoal has been met in the current world.
     *
     * @param dynamicWorld
     */
    @Override
    public void nextWorld(vision.DynamicWorld dynamicWorld) {
        DynamicWorld latestWorld = worldCalculator.getCalculatedWorld();
        if (friend1_GoalQueue.size() == 0 && friend2_GoalQueue.size() == 0) {
            return;
        }

        // Updates the current goal for FRIEND1
        f1_currentGoalIndex = 0;
        for (Goal subgoal : friend1_GoalQueue) {
            if (subgoal.wasExecuted(latestWorld)) {
                f1_currentGoalIndex++;
            } else {
                break;
            }
        }
        // and for FRIEND2
        f2_currentGoalIndex = 0;
        for (Goal subgoal : friend2_GoalQueue) {
            if (subgoal.wasExecuted(latestWorld)) {
                f2_currentGoalIndex++;
            } else {
                break;
            }
        }

        // Checks whether a turnover has happened.
        if (attacking) {
            // If one of the enemies has the ball, start defending
            if (latestWorld.getProbableBallHolder() == RobotType.FOE_1
                    || latestWorld.getProbableBallHolder() == RobotType.FOE_2) {
                defend();
                return;
            }

            // If the ball is in the FOEs' def. area
            VectorGeometry ball = latestWorld.getBall().location;
            if (ball != null) {
                if (Geometry.inRegion(ball, latestWorld.FOE_DEF_TOP_LEFT, latestWorld.FOE_DEF_BOTTOM_RIGHT)) {

                    // and at least one FOE robot is in the def area
                    if (latestWorld.getRobot(RobotType.FOE_1) != null
                            && Geometry.inRegion(
                            latestWorld.getRobot(RobotType.FOE_1).location,
                            latestWorld.FOE_DEF_TOP_LEFT,
                            latestWorld.FOE_DEF_BOTTOM_RIGHT)) {

                        // proceeds to defend
                        defend();
                        return;
                    }

                    if (latestWorld.getRobot(RobotType.FOE_2) != null
                            && Geometry.inRegion(
                            latestWorld.getRobot(RobotType.FOE_2).location,
                            latestWorld.FOE_DEF_TOP_LEFT,
                            latestWorld.FOE_DEF_BOTTOM_RIGHT)) {

                        defend();
                        return;
                    }

                    //If the ball is in Def Area and no opponent is present. Waits until something happens
                }
            }

            //TODO: use dynamic vision to check whether the ball is stationary. if so, go GetIt

            // Else if defending
        } else {
            // If one of FRIENDs has the ball
            if (latestWorld.robotWithBall == RobotType.FRIEND_1
                    || latestWorld.robotWithBall == RobotType.FRIEND_2) {

                // Starts attacking
                attack();
                return;
            }

            // If the ball is in FRIEND def area
            VectorGeometry ball = latestWorld.getBall().location;
            if (ball != null) {
                if (Geometry.inRegion(ball, latestWorld.FRIEND_DEF_TOP_LEFT, latestWorld.FRIEND_DEF_BOTTOM_RIGHT)) {

                    // Starts attacking
                    attack();
                    return;
                }
            }

            //TODO: what if the ball is stationary and we are defending?
        }
    }
}
