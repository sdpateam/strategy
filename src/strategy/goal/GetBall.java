package strategy.goal;

import strategy.commons.DynamicWorld;
import vision.RobotType;
import vision.tools.VectorGeometry;

/**
 * Part of project strategy
 * <p/>
 * Created by Jano Horvath on 27/02/16.
 */
public class GetBall extends Goal {
    public GetBall(RobotType robot) {
        super(GoalType.GET_BALL, robot);
    }

    @Override
    public VectorGeometry getTargetPoint(DynamicWorld world) {
        return world.getBall().location;
    }

    @Override
    public boolean wasExecuted(DynamicWorld world) {
        return world.getProbableBallHolder() == this.robot;
    }
}
