package strategy.goal;

/**
 * Part of project strategy
 * <p/>
 * Created by Jano Horvath on 23/02/16.
 */
public enum GoalType {
    // TODO: extend this further. The more detail we have, the better the gameplay we'll have.

    SHOOT,
    STOP,
    MAKE_PASS,
    RECEIVE_PASS,


    GOTO,
    GET_BALL,

    GOALIE,
    BLOCK_PASS,

    PENALTY,
    KICKOFF
}
