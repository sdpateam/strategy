package strategy;

import strategy.action.*;
import strategy.commons.DynamicWorld;
import java.util.List;

public class Plan {

    public DynamicWorld world;
    public List<Action>    friend1_plan;
    public List<Action>    friend2_plan;
    public Double		   cost;

    public Plan(DynamicWorld world, List<Action> friend1_plan, List<Action> friend2_plan, Double cost) {
        this.world 		  = world;
        this.friend1_plan = friend1_plan;
        this.friend2_plan = friend2_plan;
        this.cost 		  = cost;
    }
}