package strategy.tests;

import strategy.commons.DynamicWorld;
import vision.VisionListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Part of project strategy
 *
 * Created by Jano Horvath on 12/03/16.
 */
public class Planner_tests {
    private static List<VisionListener> visionListeners = new ArrayList<>();

    public static void main(String[] args) {

        // Initializes worlds which can be used as a test
        TestWorlds tests = new TestWorlds();

        DynamicWorld world = tests.getBasicTestWorld();
        System.out.println("f");

//        // Initializes all the modules
//        WorldCalculator worldCalculator = new WorldCalculator();
//        visionListeners.add(worldCalculator);
//
//        GoalCalculator goalCalculator = new GoalCalculator(worldCalculator);
//        visionListeners.add(goalCalculator);
//
//        Executor executor = new Executor(worldCalculator);
//
//        List<RobotType> activeRobots = new ArrayList<>();
//        activeRobots.add(RobotType.FRIEND_1);
//
//        Planner planner = new Planner(goalCalculator, worldCalculator, executor, activeRobots);
//        visionListeners.add(planner);
//
//        /**
//         * Begin test
//         */
//        // Distributes initial world
//        notifyListeners(tests.getBasicTestWorld());
//        // Sets goal(s) to robot(s)
//        goalCalculator.setMainGoal(new Shoot(RobotType.FRIEND_1), RobotType.FRIEND_1);
//        // Gets plan for that goal in that world
//        Plan plan = planner.getPlan(activeRobots);
//
//        // Prints plan(s) for robot(s)
//        System.out.println("Plan for robot 1:");
//        for (Action action : plan.friend1_plan) {
//            System.out.print(action.getClass().getSimpleName());
//            System.out.print(" -> ");
//        }
//
//        System.out.println("Plan for robot 2:");
//        for (Action action : plan.friend2_plan) {
//            System.out.print(action.getClass().getSimpleName());
//            System.out.print(" -> ");
//        }

    }

    private static void notifyListeners(DynamicWorld calculatedWorld) {
        for (VisionListener listener : visionListeners) {
            listener.nextWorld(calculatedWorld);
        }
    }
}
